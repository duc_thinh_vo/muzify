import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  apiUrl = environment.API_BASE_URL;
  signUpViaFacebookUrl = `${this.apiUrl}/users`;

  constructor(public http: HttpClient) {}

  public loginViaFacebook(token) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', environment.Authorization)

    const body = new HttpParams()
      .set('access_token', token);

    return this.http.post(this.signUpViaFacebookUrl, body.toString(),{ headers: headers });
  }
}

