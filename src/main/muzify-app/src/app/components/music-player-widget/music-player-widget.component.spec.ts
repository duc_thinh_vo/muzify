import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicPlayerWidgetComponent } from './music-player-widget.component';

describe('MusicPlayerWidgetComponent', () => {
  let component: MusicPlayerWidgetComponent;
  let fixture: ComponentFixture<MusicPlayerWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicPlayerWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicPlayerWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
