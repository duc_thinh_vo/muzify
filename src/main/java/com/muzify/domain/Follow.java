/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thinh
 */
@Entity
@Table(name = "follow")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Follow.findAll", query = "SELECT f FROM Follow f")
    , @NamedQuery(name = "Follow.findByFollowerId", query = "SELECT f FROM Follow f WHERE f.followPK.followerId = :followerId")
    , @NamedQuery(name = "Follow.findByFolloweeId", query = "SELECT f FROM Follow f WHERE f.followPK.followeeId = :followeeId")
    , @NamedQuery(name = "Follow.findByCreatedAt", query = "SELECT f FROM Follow f WHERE f.createdAt = :createdAt")})
public class Follow implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FollowPK followPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @JoinColumn(name = "follower_id", referencedColumnName = "uuid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private OauthUser oauthUser;
    @JoinColumn(name = "followee_id", referencedColumnName = "uuid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private OauthUser oauthUser1;

    public Follow() {
    }

    public Follow(FollowPK followPK) {
        this.followPK = followPK;
    }

    public Follow(FollowPK followPK, Date createdAt) {
        this.followPK = followPK;
        this.createdAt = createdAt;
    }

    public Follow(String followerId, String followeeId) {
        this.followPK = new FollowPK(followerId, followeeId);
    }

    public FollowPK getFollowPK() {
        return followPK;
    }

    public void setFollowPK(FollowPK followPK) {
        this.followPK = followPK;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public OauthUser getOauthUser() {
        return oauthUser;
    }

    public void setOauthUser(OauthUser oauthUser) {
        this.oauthUser = oauthUser;
    }

    public OauthUser getOauthUser1() {
        return oauthUser1;
    }

    public void setOauthUser1(OauthUser oauthUser1) {
        this.oauthUser1 = oauthUser1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (followPK != null ? followPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Follow)) {
            return false;
        }
        Follow other = (Follow) object;
        if ((this.followPK == null && other.followPK != null) || (this.followPK != null && !this.followPK.equals(other.followPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.muzify.domain.Follow[ followPK=" + followPK + " ]";
    }
    
}
