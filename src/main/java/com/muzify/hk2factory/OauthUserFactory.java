/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.hk2factory;
import com.muzify.domain.OauthUser;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import org.glassfish.hk2.api.Factory;
/**
 *
 * @author thinh
 */
public class OauthUserFactory implements Factory<OauthUser> {
    public static final String OauthUserKey = "MuzifyOauthUserKey";
    private final ContainerRequestContext context;
    
    @Inject
    public OauthUserFactory(ContainerRequestContext context) {
        this.context = context;
    }
    
    @Override
    public OauthUser provide() {
        return (OauthUser)this.context.getProperty(OauthUserKey);
    }

    @Override
    public void dispose(OauthUser t) {}
}
