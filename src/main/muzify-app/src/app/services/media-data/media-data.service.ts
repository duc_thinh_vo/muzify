import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {UserSession} from '../../UserSession';

@Injectable({
  providedIn: 'root'
})
export class MediaDataService {
  apiUrl = environment.API_BASE_URL;
  uploadUrl = `${this.apiUrl}/songs`;
  mediasOfCurrentUserUrl = `${this.apiUrl}/songs/me`;

  constructor(public http: HttpClient) { }

  public uploadMedia(formData: FormData) {
    return this.http.post(this.uploadUrl, formData);
  }

  public getMediasOfCurrentUser() {
    const headers = new HttpHeaders()
      .set('Authorization', UserSession.accessToken)

    return this.http.get(this.mediasOfCurrentUserUrl, { headers: headers });
  }

//   public getMediaOfCurrentUser() {
//     return this.http.get(this.mediasOfCurrentUserUrl);
//   }
}
