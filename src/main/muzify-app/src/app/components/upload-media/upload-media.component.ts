import { Component, OnInit } from '@angular/core';
import { MediaDataService } from '../../services/media-data/media-data.service';
import { UserDataService } from '../../services/user-data/user-data.service';
import { Media } from '../../models/media';

@Component({
  selector: 'app-upload-media',
  templateUrl: './upload-media.component.html',
  styleUrls: ['./upload-media.component.scss']
})
export class UploadMediaComponent implements OnInit {

  constructor(private mediaDataService: MediaDataService,
              private userDataService: UserDataService) { }

  media: any = {
    name: '',
    file: null
  };

  ngOnInit() {
  }

  uploadMedia() {
    this.mediaDataService.uploadMedia(this.media);
  }
}
