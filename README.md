# Muzify
---
Table of contents

[1. Introduction](#introduction)  
[2. Requirement](#requirements)

## 1. Introduction
---

Muzify is a music social media platform that allows user to store and share their own music.  
This repository includes both front end and backend source code.

You can clone and play around with this project:  
`git clone https://duc_thinh_vo@bitbucket.org/duc_thinh_vo/muzify.git`

## 2. Requirements
---
  * Netbeans IDE  
  * JDK 1.8+  
  * Maven  

Maven is used to migrate the database by using Flyway dependency.  
If you do not have Maven install on your computer, you can follow this [link](https://maven.apache.org/install.html).  

If you are a Mac OS user and you have [Homebrew](https://brew.sh/) installed on your computer. You can install Maven by typing the following into your terminal:  
`brew install maven`  

Once you finished installing Maven, you are one step behind the database migration.  
Now, open __flyway.properties__ file and make changes to the database credentials according to your own database setup such as database name(schema), user, password, etc.  

In order to start the migration. Open *terminal* or *shell* at the project's root directory, paste the following:  
`mvn clean flyway:migrate -Dflyway.configFile=flyway.properties`  

If are unable to execute the above command due to the reason that *mvn command is not found* on a Mac, Linux, you can do:  
`export PATH=/usr/local/Cellar/maven/3.5.2/bin/:$PATH` (where 3.5.2 is the version and this could be different on your machine)   
Or double check the installation process.  

After successfully doing the database migration, you are ready to run the project.  
