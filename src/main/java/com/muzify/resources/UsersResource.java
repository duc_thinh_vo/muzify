/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

import com.muzify.dao.OauthAccessTokenFacade;
import com.muzify.dao.OauthRefreshTokenFacade;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.muzify.dao.OauthUserFacade;
import com.muzify.domain.OauthAccessToken;
import com.muzify.domain.OauthClient;
import com.muzify.domain.OauthRefreshToken;
import com.muzify.domain.OauthTokenResponse;
import com.muzify.domain.OauthUser;
import com.muzify.dynamicfeature.ClientAuthenticated;
import com.muzify.dynamicfeature.TokenAuthenticated;
import com.muzify.exception.ErrorMessage;
import com.muzify.exception.MissingParamException;
import com.muzify.exception.NotAuthorizedException;
import com.muzify.exception.NotFoundException;
import com.muzify.utils.DateUtil;
import com.muzify.utils.FacebookInfoHelper;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.container.ContainerRequestContext;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

/**
 * REST Web Service
 *
 * @author thinh
 */
@Stateless
@Path("/users")
@Api(value = "/users", description="User endpoints")
public class UsersResource {
    /**
     * TODO: Move all handling code to UserService
     */
    
    @Context
    private UriInfo context;
    
    @EJB
    OauthUserFacade oauthUserFacade;
    
    @EJB
    OauthAccessTokenFacade accessTokenFacade;
    
    @EJB
    OauthRefreshTokenFacade refreshTokenFacade;
    
    @GET
    @TokenAuthenticated
    @Path("{uuid}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Gets a specific user", notes = "Fetches the user's details")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 401, message = "Unauthorized request"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response getUser(@PathParam("uuid") String uuid) throws NotFoundException {
        
        OauthUser user = oauthUserFacade.find(uuid);
        if (user == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Unable to find user by the given uuid", "", Response.Status.NOT_FOUND);
            throw new NotFoundException(errorMessage);
        }
        return Response.status(200).entity("sdfa").build();
    }
    
    @POST
    @ClientAuthenticated
    @Path("")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Signin/Signup", notes = "Sign in or sign up by a Facebook access token. Content-type: application/x-www-form-urlencoded")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response post(@FormParam("access_token") String accessToken, @Context ContainerRequestContext requestContext) throws NotFoundException, OAuthSystemException, OAuthProblemException, IOException {
        // Request to facebook server to get user info
        if (accessToken == null || accessToken.equals("")) {
            ErrorMessage errorMessage = new ErrorMessage("", "Missing Facebook access token", "", Response.Status.BAD_REQUEST);
            throw new MissingParamException(errorMessage);
        }
        
        OauthUser facebookUser = FacebookInfoHelper.oauthUserForAccessToken(accessToken);
        OauthUser signedUpUser = oauthUserFacade.findUserByEmail(facebookUser.getEmail());
           
        if (signedUpUser == null) {
            // Create a new user
            oauthUserFacade.create(facebookUser);
            signedUpUser = facebookUser;
            
        } else {
            // Update access token
            signedUpUser.setFbAccessToken(accessToken);
            oauthUserFacade.edit(signedUpUser);
        }
        
        OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
        
        OauthClient client = (OauthClient)requestContext.getProperty("client");
        String clientId = client.getClientId();
        
        // Generate access token
        OauthAccessToken generatedAccessToken = new OauthAccessToken();
        generatedAccessToken.setClientId(clientId);
        generatedAccessToken.setAccessToken(oauthIssuerImpl.accessToken());
        generatedAccessToken.setUserUuid(signedUpUser);
        generatedAccessToken.setScope("all");
        generatedAccessToken.setExpires(DateUtil.addHours(new Date(), 1));
        accessTokenFacade.create(generatedAccessToken);
        
        // RefreshToken
        OauthRefreshToken refreshToken = refreshTokenFacade.findRefeshTokenForUserAndClientId(signedUpUser, clientId);
        if (refreshToken == null) {
            refreshToken = new OauthRefreshToken();
            refreshToken.setClientId(clientId);
            refreshToken.setUserUuid(signedUpUser);
            refreshToken.setScope("all");
            refreshToken.setRefreshToken(oauthIssuerImpl.refreshToken());
            refreshTokenFacade.create(refreshToken);
        }
        
        OauthTokenResponse response = new OauthTokenResponse();
        response.setAccessToken(generatedAccessToken.getAccessToken());
        response.setExpire(3600);
        response.setScope(generatedAccessToken.getScope());
        response.setUser(signedUpUser);
        response.setTokenType("Bearer");
        response.setRefreshToken(refreshToken.getRefreshToken());
        
        return Response.status(200).entity(response).build();
    }
    
        
    @PUT
    @TokenAuthenticated
    @Path("/me")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Updates user information", notes = "Update users information")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Updated successfully"),
        @ApiResponse(code = 403, message = "Unable to update"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response put(@FormParam("age") Integer age, 
                        @FormParam("full_name") String fullName,
                        @FormParam("gender") String gender,
                        @FormParam("city") String city,
                        @FormParam("description") String description,
                        @FormParam("language") String language) {
        
        // Replace this by selecting user from Oauth2 filter
        OauthUser user = oauthUserFacade.find("65eb8489-6c94-490e-b892-68604f38a5af");
        
        if (age != null) {
            user.setAge(age);
        }
        
        if (fullName != null) {
            user.setFullName(fullName);
        }
        
        if (gender != null) {
            user.setGender(gender);
        }
        
        if (description != null) {
            user.setDescription(description);
        }
        
        if (city != null) {
            user.setCity(city);
        }
        
        if (language != null) {
            user.setLanguage(language);
        }
        
        oauthUserFacade.edit(user);
        return Response.status(204).entity(user).build();
    }
    
    @DELETE
    @Path("{user_uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Detetes a user", notes = "Delete user by the user")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Deleted successfully"),
        @ApiResponse(code = 401, message = "Unable to delete"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response delete(@PathParam("user_uuid") String uuid) {
        // Replace this by selecting user from Oauth2 filter
        OauthUser user = oauthUserFacade.find(uuid);
        
        if (user == null) {
            // Not allow to delete
            ErrorMessage errorMessage = new ErrorMessage("", "Unable to find user by the given UUID", "", Response.Status.BAD_REQUEST);
            throw new NotFoundException(errorMessage);
        }
        
        // Compare filter user's uuid to uuid parameter
        if (user.getUuid().equals(uuid)) {
            // Delete user 
            oauthUserFacade.remove(user);
            return Response.status(204).entity(null).build();
        }
        
        ErrorMessage errorMessage = new ErrorMessage("", "Unable to delete user", "", Response.Status.UNAUTHORIZED);
        throw new NotAuthorizedException(errorMessage);
    }
}
