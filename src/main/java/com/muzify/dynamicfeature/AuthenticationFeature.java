/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.dynamicfeature;

import com.muzify.filter.BearerTokenFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author thinh
 */
@Provider
public class AuthenticationFeature implements DynamicFeature {

    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        TokenAuthenticated annotation = resourceInfo.getResourceMethod().getAnnotation(TokenAuthenticated.class);
        if (annotation == null) {return;}
        BearerTokenFilter bearerTokenFilter = new BearerTokenFilter();
        context.register(bearerTokenFilter);
    }
    
}
