/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author thinh
 */
@Entity
@Table(name = "oauth_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OauthUser.findAll", query = "SELECT o FROM OauthUser o")
    , @NamedQuery(name = "OauthUser.findByUuid", query = "SELECT o FROM OauthUser o WHERE o.uuid = :uuid")
    , @NamedQuery(name = "OauthUser.findByUsername", query = "SELECT o FROM OauthUser o WHERE o.username = :username")
    , @NamedQuery(name = "OauthUser.findByPassword", query = "SELECT o FROM OauthUser o WHERE o.password = :password")
    , @NamedQuery(name = "OauthUser.findByEmail", query = "SELECT o FROM OauthUser o WHERE o.email = :email")
    , @NamedQuery(name = "OauthUser.findByUsertype", query = "SELECT o FROM OauthUser o WHERE o.usertype = :usertype")
    , @NamedQuery(name = "OauthUser.findByCreated", query = "SELECT o FROM OauthUser o WHERE o.created = :created")
    , @NamedQuery(name = "OauthUser.findByTotalFollowing", query = "SELECT o FROM OauthUser o WHERE o.totalFollowing = :totalFollowing")
    , @NamedQuery(name = "OauthUser.findByTotalFollowers", query = "SELECT o FROM OauthUser o WHERE o.totalFollowers = :totalFollowers")
    , @NamedQuery(name = "OauthUser.findByFbId", query = "SELECT o FROM OauthUser o WHERE o.fbId = :fbId")
    , @NamedQuery(name = "OauthUser.findByFullName", query = "SELECT o FROM OauthUser o WHERE o.fullName = :fullName")
    , @NamedQuery(name = "OauthUser.findByLanguage", query = "SELECT o FROM OauthUser o WHERE o.language = :language")
    , @NamedQuery(name = "OauthUser.findByCountryCode", query = "SELECT o FROM OauthUser o WHERE o.countryCode = :countryCode")
    , @NamedQuery(name = "OauthUser.findByAge", query = "SELECT o FROM OauthUser o WHERE o.age = :age")
    , @NamedQuery(name = "OauthUser.findByGender", query = "SELECT o FROM OauthUser o WHERE o.gender = :gender")
    , @NamedQuery(name = "OauthUser.findByCity", query = "SELECT o FROM OauthUser o WHERE o.city = :city")
    , @NamedQuery(name = "OauthUser.findByDateOfBirth", query = "SELECT o FROM OauthUser o WHERE o.dateOfBirth = :dateOfBirth")
    , @NamedQuery(name = "OauthUser.findByVerified", query = "SELECT o FROM OauthUser o WHERE o.verified = :verified")
    , @NamedQuery(name = "OauthUser.findByDiscoverable", query = "SELECT o FROM OauthUser o WHERE o.discoverable = :discoverable")
    , @NamedQuery(name = "OauthUser.findByAccountStatus", query = "SELECT o FROM OauthUser o WHERE o.accountStatus = :accountStatus")})
public class OauthUser implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userUuid")
    private Collection<EmailVerification> emailVerificationCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Size(min = 1, max = 36)
    @Column(name = "uuid")
    private String uuid;
    @Size(max = 255)
    @Column(name = "username")
    private String username;
    @Size(max = 2000)
    @Column(name = "password")
    private String password;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    @Lob
    @Size(max = 65535)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @Size(min = 1, max = 9)
    @Column(name = "usertype")
    private String usertype;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total_following")
    private int totalFollowing;
    @Basic(optional = false)
    @NotNull
    @Column(name = "total_followers")
    private int totalFollowers;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @Column(name = "fb_id")
    private BigInteger fbId;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "fb_access_token")
    private String fbAccessToken;
    @Size(max = 255)
    @Column(name = "full_name")
    private String fullName;
    @Size(max = 2)
    @Column(name = "language")
    private String language;
    @Size(max = 2)
    @Column(name = "country_code")
    private String countryCode;
    @Column(name = "age")
    private Integer age;
    @Size(max = 20)
    @Column(name = "gender")
    private String gender;
    @Size(max = 255)
    @Column(name = "city")
    private String city;
    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
    @Basic(optional = false)
    @Column(name = "verified")
    private boolean verified;
    @Basic(optional = false)
    @NotNull
    @Column(name = "discoverable")
    private boolean discoverable;
    @Basic(optional = false)
    @Size(min = 1, max = 11)
    @Column(name = "account_status")
    private String accountStatus;
    @XmlElement(nillable=true)
    @Lob
    @Size(max = 65535)
    @Column(name = "ip")
    private String ip;
    @OneToMany(mappedBy = "userUuid")
    private Collection<UserProfilePhoto> userProfilePhotoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oauthUser")
    private Collection<Follow> followCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oauthUser1")
    private Collection<Follow> followCollection1;
    @OneToMany(mappedBy = "userUuid")
    private Collection<OauthAuthorizationCodes> oauthAuthorizationCodesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oauthUser")
    private Collection<LikeToPost> likeToPostCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userUuid")
    private Collection<Playlist> playlistCollection;
    @OneToMany(mappedBy = "creatorUuid")
    private Collection<Post> postCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userUuid")
    private Collection<SongOwner> songOwnerCollection;
    @OneToMany(mappedBy = "byUserId")
    private Collection<Lyric> lyricCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userUuid")
    private Collection<Comment> commentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userUuid")
    private Collection<OauthAccessToken> oauthAccessTokenCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userUuid")
    private Collection<OauthRefreshToken> oauthRefreshTokenCollection;
    @ManyToMany(fetch=FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name="song_owner", joinColumns=@JoinColumn(name="user_uuid", referencedColumnName="uuid"), inverseJoinColumns=@JoinColumn(name="song_id", referencedColumnName="id"))
    private List<Song> songs;
    
    public OauthUser() {
    }

    public OauthUser(String uuid) {
        this.uuid = uuid;
    }

    public OauthUser(String uuid, String usertype, Date created, int totalFollowing, int totalFollowers, String fbAccessToken, boolean verified, boolean discoverable, String accountStatus) {
        this.uuid = uuid;
        this.usertype = usertype;
        this.created = created;
        this.totalFollowing = totalFollowing;
        this.totalFollowers = totalFollowers;
        this.fbAccessToken = fbAccessToken;
        this.verified = verified;
        this.discoverable = discoverable;
        this.accountStatus = accountStatus;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @XmlTransient
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getTotalFollowing() {
        return totalFollowing;
    }

    public void setTotalFollowing(int totalFollowing) {
        this.totalFollowing = totalFollowing;
    }

    public int getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(int totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigInteger getFbId() {
        return fbId;
    }

    public void setFbId(BigInteger fbId) {
        this.fbId = fbId;
    }

    public String getFbAccessToken() {
        return fbAccessToken;
    }

    public void setFbAccessToken(String fbAccessToken) {
        this.fbAccessToken = fbAccessToken;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean getVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean getDiscoverable() {
        return discoverable;
    }

    public void setDiscoverable(boolean discoverable) {
        this.discoverable = discoverable;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }
    
    @XmlElement(nillable=true)
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @XmlTransient
    public Collection<UserProfilePhoto> getUserProfilePhotoCollection() {
        return userProfilePhotoCollection;
    }

    public void setUserProfilePhotoCollection(Collection<UserProfilePhoto> userProfilePhotoCollection) {
        this.userProfilePhotoCollection = userProfilePhotoCollection;
    }

    @XmlTransient
    public Collection<Follow> getFollowCollection() {
        return followCollection;
    }

    public void setFollowCollection(Collection<Follow> followCollection) {
        this.followCollection = followCollection;
    }

    @XmlTransient
    public Collection<Follow> getFollowCollection1() {
        return followCollection1;
    }

    public void setFollowCollection1(Collection<Follow> followCollection1) {
        this.followCollection1 = followCollection1;
    }

    @XmlTransient
    public Collection<OauthAuthorizationCodes> getOauthAuthorizationCodesCollection() {
        return oauthAuthorizationCodesCollection;
    }

    public void setOauthAuthorizationCodesCollection(Collection<OauthAuthorizationCodes> oauthAuthorizationCodesCollection) {
        this.oauthAuthorizationCodesCollection = oauthAuthorizationCodesCollection;
    }

    @XmlTransient
    public Collection<LikeToPost> getLikeToPostCollection() {
        return likeToPostCollection;
    }

    public void setLikeToPostCollection(Collection<LikeToPost> likeToPostCollection) {
        this.likeToPostCollection = likeToPostCollection;
    }

    @XmlTransient
    public Collection<Playlist> getPlaylistCollection() {
        return playlistCollection;
    }

    public void setPlaylistCollection(Collection<Playlist> playlistCollection) {
        this.playlistCollection = playlistCollection;
    }

    @XmlTransient
    public Collection<Post> getPostCollection() {
        return postCollection;
    }

    public void setPostCollection(Collection<Post> postCollection) {
        this.postCollection = postCollection;
    }

    @XmlTransient
    public Collection<SongOwner> getSongOwnerCollection() {
        return songOwnerCollection;
    }

    public void setSongOwnerCollection(Collection<SongOwner> songOwnerCollection) {
        this.songOwnerCollection = songOwnerCollection;
    }

    @XmlTransient
    public Collection<Lyric> getLyricCollection() {
        return lyricCollection;
    }

    public void setLyricCollection(Collection<Lyric> lyricCollection) {
        this.lyricCollection = lyricCollection;
    }

    @XmlTransient
    public Collection<Comment> getCommentCollection() {
        return commentCollection;
    }

    public void setCommentCollection(Collection<Comment> commentCollection) {
        this.commentCollection = commentCollection;
    }

    @XmlTransient
    public Collection<OauthAccessToken> getOauthAccessTokenCollection() {
        return oauthAccessTokenCollection;
    }

    public void setOauthAccessTokenCollection(Collection<OauthAccessToken> oauthAccessTokenCollection) {
        this.oauthAccessTokenCollection = oauthAccessTokenCollection;
    }

    @XmlTransient
    public Collection<OauthRefreshToken> getOauthRefreshTokenCollection() {
        return oauthRefreshTokenCollection;
    }

    public void setOauthRefreshTokenCollection(Collection<OauthRefreshToken> oauthRefreshTokenCollection) {
        this.oauthRefreshTokenCollection = oauthRefreshTokenCollection;
    }
    
    @XmlTransient
    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (uuid != null ? uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OauthUser)) {
            return false;
        }
        OauthUser other = (OauthUser) object;
        if ((this.uuid == null && other.uuid != null) || (this.uuid != null && !this.uuid.equals(other.uuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.muzify.domain.OauthUser[ uuid=" + uuid + " ]";
    }
    
    @PrePersist
    public void prePersist() {
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
        }
        
        if (created == null) {
            created = new Date();
        }
        
        if (usertype == null) {
            usertype = "user";
        }
        
        if (accountStatus == null) {
            accountStatus = "active";
        }
    }

    @XmlTransient
    public Collection<EmailVerification> getEmailVerificationCollection() {
        return emailVerificationCollection;
    }

    public void setEmailVerificationCollection(Collection<EmailVerification> emailVerificationCollection) {
        this.emailVerificationCollection = emailVerificationCollection;
    }
}
