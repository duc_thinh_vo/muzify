/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.exception;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author thinh
 */
@Provider
public class NotAuthorizedException extends WebApplicationException {
    
    /**
    * Create a HTTP 401 (Not Authorized) exception.
    * @param message the String that is the entity of the 401 response.
    */
    public NotAuthorizedException(ErrorMessage message) {
        super(Response.status(Response.Status.UNAUTHORIZED).
        entity(message.toJSON()).type(MediaType.APPLICATION_JSON).build());
    }
}
