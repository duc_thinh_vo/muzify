/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

import com.muzify.dao.ArtistFacade;
import com.muzify.dao.GenreFacade;
import com.muzify.dao.OauthUserFacade;
import com.muzify.dao.SongFacade;
import com.muzify.dao.SongOwnerFacade;
import com.muzify.domain.Artist;
import com.muzify.domain.Genre;
import com.muzify.domain.OauthUser;
import com.muzify.domain.Song;
import com.muzify.domain.SongOwner;
import com.muzify.dynamicfeature.TokenAuthenticated;
import com.muzify.exception.ErrorMessage;
import com.muzify.exception.MissingParamException;
import com.muzify.exception.NotAuthorizedException;
import com.muzify.exception.NotFoundException;
import com.muzify.hk2factory.OauthUserFactory;
import com.muzify.utils.AudioFileManager;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 * REST Web Service
 *
 * @author thinh
 */
@Stateless
@Path("songs")
@Api(value = "/songs", description="Song endpoints")
public class SongsResource {
    final String testUser = "49d186d6-b592-407a-8ab7-8cec9fbe1a32";
    
    @Context
    private UriInfo context;
    
    @EJB
    OauthUserFacade oauthUserFacade;
    
    @EJB
    GenreFacade genreFacade;
    
    @EJB
    ArtistFacade artistFacade;
    
    @EJB
    SongFacade songFacade;
    
    @EJB
    SongOwnerFacade songOwnerFacade;
    
    public SongsResource() {
    }
    
    @GET
    @TokenAuthenticated
    @Path("me")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSongsForAuthorizedUser(@Context ContainerRequestContext requestContext) {
        OauthUser currentUser = (OauthUser)requestContext.getProperty(OauthUserFactory.OauthUserKey);
        if (currentUser == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Unauthorized", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        List<Song> songs = currentUser.getSongs();
        GenericEntity<List<Song>> genericEntity = new GenericEntity<List<Song>>(songs){};
        return Response.status(200).entity(genericEntity).build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
    @POST
    @TokenAuthenticated
    @ApiOperation(value = "Creates a song", notes = "Creates a song")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 401, message = "Unauthorized request"),
        @ApiResponse(code = 500, message = "Internal server error")})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postSong(
                         @FormDataParam("length") Float length,
                         @FormDataParam("genre_id") Integer genreId,
                         @FormDataParam("file") InputStream inputStream,
                         @FormDataParam("file") FormDataContentDisposition fileDetail,
                         @Context ContainerRequestContext requestContext) throws MissingParamException, MissingParamException {
        OauthUser currentUser = (OauthUser)requestContext.getProperty(OauthUserFactory.OauthUserKey);
        if (currentUser != null) {
            if (inputStream != null && fileDetail != null) {
                AudioFileManager ufm = new AudioFileManager();
                String fileName = fileDetail.getFileName();
                String fileExtension = ufm.getFileExtension(fileName);
                String randomFileName = UUID.randomUUID().toString() + "." + fileExtension;
                try {
                    ufm.writeFileToDisk(inputStream, randomFileName, true);
                } catch (IOException e) {
                    return Response.status(500).entity("Internal server error.").build();
                }
                
                String songURL = context.getBaseUri().toString() + "audio/" + randomFileName;
                
                Song song = new Song();
                song.setUrl(songURL);
                song.setName(ufm.getFileNameWithoutExtension(fileName));
                
                if (length != null) { song.setLength(length); }
                if (genreId != null) {
                    Genre genre = genreFacade.find(genreId);
                    if (genre != null) {
                        song.setGenreId(genre);
                    }
                }

                songFacade.create(song);
                
                List<OauthUser> owners = new ArrayList<OauthUser>();
                owners.add(currentUser);
                song.setOwners(owners);
                
                songFacade.edit(song);
                //song.setSongOwnerCollection(songOwnerCollection);
                
                
                
                List<Song> currentUserSongs = currentUser.getSongs();
                currentUserSongs.add(song);
                currentUser.setSongs(currentUserSongs);
                
                oauthUserFacade.edit(currentUser);
                
                return Response.status(200).entity(song).build();
            }
            
            ErrorMessage errorMessage = new ErrorMessage("", "Provide at least the file", "", Response.Status.BAD_REQUEST);
            throw new MissingParamException(errorMessage);
        }
        
        return Response.status(500).entity("Internal server error. Unable to upload the file.").build();
    }
}
