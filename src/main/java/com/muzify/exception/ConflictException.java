/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
/**
 *
 * @author thinh
 */
/**
    * Create a HTTP 409 (Conflict) exception.
    * @param message the String that is the entity of the 409 response.
    */
@Provider
public class ConflictException extends WebApplicationException {
    public ConflictException(ErrorMessage messagge) {
        super(Response.status(Response.Status.CONFLICT).
                entity(messagge.toJSON()).type(MediaType.APPLICATION_JSON).build());
    }
}
