/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.dynamicfeature;

import com.muzify.filter.BasicAuthorizationFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author thinh
 */
@Provider
public class BasicAuthorizationFeature implements DynamicFeature {

    @Override
    public void configure(ResourceInfo ri, FeatureContext fc) {
        ClientAuthenticated annotation = ri.getResourceMethod().getDeclaredAnnotation(ClientAuthenticated.class);
        if (annotation == null) { return; }
        BasicAuthorizationFilter filter = new BasicAuthorizationFilter();
        fc.register(filter);
    }
    
}
