/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thinh
 */
@Entity
@Table(name = "like_to_post")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LikeToPost.findAll", query = "SELECT l FROM LikeToPost l")
    , @NamedQuery(name = "LikeToPost.findByUserUuid", query = "SELECT l FROM LikeToPost l WHERE l.likeToPostPK.userUuid = :userUuid")
    , @NamedQuery(name = "LikeToPost.findByPostId", query = "SELECT l FROM LikeToPost l WHERE l.likeToPostPK.postId = :postId")
    , @NamedQuery(name = "LikeToPost.findByCreatedAt", query = "SELECT l FROM LikeToPost l WHERE l.createdAt = :createdAt")})
public class LikeToPost implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LikeToPostPK likeToPostPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @JoinColumn(name = "user_uuid", referencedColumnName = "uuid", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private OauthUser oauthUser;
    @JoinColumn(name = "post_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Post post;

    public LikeToPost() {
    }

    public LikeToPost(LikeToPostPK likeToPostPK) {
        this.likeToPostPK = likeToPostPK;
    }

    public LikeToPost(LikeToPostPK likeToPostPK, Date createdAt) {
        this.likeToPostPK = likeToPostPK;
        this.createdAt = createdAt;
    }

    public LikeToPost(String userUuid, long postId) {
        this.likeToPostPK = new LikeToPostPK(userUuid, postId);
    }

    public LikeToPostPK getLikeToPostPK() {
        return likeToPostPK;
    }

    public void setLikeToPostPK(LikeToPostPK likeToPostPK) {
        this.likeToPostPK = likeToPostPK;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public OauthUser getOauthUser() {
        return oauthUser;
    }

    public void setOauthUser(OauthUser oauthUser) {
        this.oauthUser = oauthUser;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (likeToPostPK != null ? likeToPostPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LikeToPost)) {
            return false;
        }
        LikeToPost other = (LikeToPost) object;
        if ((this.likeToPostPK == null && other.likeToPostPK != null) || (this.likeToPostPK != null && !this.likeToPostPK.equals(other.likeToPostPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.muzify.domain.LikeToPost[ likeToPostPK=" + likeToPostPK + " ]";
    }
    
}
