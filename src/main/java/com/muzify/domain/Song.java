/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author thinh
 */
@Entity
@Table(name = "song")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Song.findAll", query = "SELECT s FROM Song s")
    , @NamedQuery(name = "Song.findById", query = "SELECT s FROM Song s WHERE s.id = :id")
    , @NamedQuery(name = "Song.findByName", query = "SELECT s FROM Song s WHERE s.name = :name")
    , @NamedQuery(name = "Song.findByLength", query = "SELECT s FROM Song s WHERE s.length = :length")})
public class Song implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "length")
    private Float length;
    @Lob
    @Size(max = 65535)
    @Column(name = "url")
    private String url;
    @JoinColumn(name = "genre_id", referencedColumnName = "id")
    @ManyToOne
    private Genre genreId;
    @JoinColumn(name = "album_id", referencedColumnName = "id")
    @ManyToOne
    private Album albumId;
    @OneToMany(mappedBy = "songId")
    private Collection<SungBy> sungByCollection;
    @OneToMany(mappedBy = "songId")
    private Collection<PlaylistSong> playlistSongCollection;
    @OneToMany(mappedBy = "songId")
    private Collection<Post> postCollection;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "songId")
    private Collection<SongOwner> songOwnerCollection;
    @OneToMany(mappedBy = "songId")
    private Collection<Lyric> lyricCollection;
    @OneToMany(mappedBy = "songId")
    private Collection<SongImage> songImageCollection;
    @ManyToMany(mappedBy="songs", cascade={CascadeType.PERSIST, CascadeType.MERGE})
    private List<OauthUser> owners;
    @ManyToMany(mappedBy="songs", cascade={CascadeType.PERSIST, CascadeType.MERGE})
    private List<Playlist> playlists;
    public Song() {
    }

    public Song(Long id) {
        this.id = id;
    }

    public Song(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Genre getGenreId() {
        return genreId;
    }

    public void setGenreId(Genre genreId) {
        this.genreId = genreId;
    }

    public Album getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Album albumId) {
        this.albumId = albumId;
    }

    @XmlTransient
    public Collection<SungBy> getSungByCollection() {
        return sungByCollection;
    }

    public void setSungByCollection(Collection<SungBy> sungByCollection) {
        this.sungByCollection = sungByCollection;
    }

    @XmlTransient
    public Collection<PlaylistSong> getPlaylistSongCollection() {
        return playlistSongCollection;
    }

    public void setPlaylistSongCollection(Collection<PlaylistSong> playlistSongCollection) {
        this.playlistSongCollection = playlistSongCollection;
    }

    @XmlTransient
    public Collection<Post> getPostCollection() {
        return postCollection;
    }

    public void setPostCollection(Collection<Post> postCollection) {
        this.postCollection = postCollection;
    }

    @XmlTransient
    public Collection<SongOwner> getSongOwnerCollection() {
        return songOwnerCollection;
    }

    public void setSongOwnerCollection(Collection<SongOwner> songOwnerCollection) {
        this.songOwnerCollection = songOwnerCollection;
    }

    @XmlTransient
    public Collection<Lyric> getLyricCollection() {
        return lyricCollection;
    }

    public void setLyricCollection(Collection<Lyric> lyricCollection) {
        this.lyricCollection = lyricCollection;
    }

    @XmlTransient
    public Collection<SongImage> getSongImageCollection() {
        return songImageCollection;
    }

    public void setSongImageCollection(Collection<SongImage> songImageCollection) {
        this.songImageCollection = songImageCollection;
    }
    
    @XmlTransient
    public List<OauthUser> getOwners() {
        return owners;
    }

    public void setOwners(List<OauthUser> owners) {
        this.owners = owners;
    }
    
    @XmlTransient
    public List<Playlist> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<Playlist> playlists) {
        this.playlists = playlists;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Song)) {
            return false;
        }
        Song other = (Song) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.muzify.domain.Song[ id=" + id + " ]";
    }
    
}
