/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.dao;

import com.muzify.domain.OauthAccessToken;
import com.muzify.domain.OauthUser;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author thinh
 */
@Stateless
public class OauthAccessTokenFacade extends AbstractFacade<OauthAccessToken> {

    @PersistenceContext(unitName = "com.mycompany_Muzify_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OauthAccessTokenFacade() {
        super(OauthAccessToken.class);
    }
    
    public OauthAccessToken findLatestAccessTokenForUser(OauthUser user) {
        try {
            
            return (OauthAccessToken)em.createNamedQuery("OauthAccessToken.selectLatestAccessTokenForUser").setParameter("userUuid", user).setMaxResults(1).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        
    }
    
    public OauthAccessToken findAccessTokenForUser(OauthUser user, String clientId) {
        try {
            return (OauthAccessToken)em.createNamedQuery("OauthAccessToken.selectLatestAccessTokenForUser").setParameter("userUuid", user).setMaxResults(1).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public OauthAccessToken findAccessToken(String token) {
        try {
            return (OauthAccessToken)em.createNamedQuery("OauthAccessToken.findByAccessToken").setParameter("accessToken", token).setMaxResults(1).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
