import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { MusicPlayerWidgetComponent } from './components/music-player-widget/music-player-widget.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { NavComponent } from './components/nav/nav.component';
import { HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { UploadMediaComponent } from './components/upload-media/upload-media.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { EmailConfirmationComponent } from './components/email-confirmation/email-confirmation.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    MusicPlayerWidgetComponent,
    FooterComponent,
    HeaderComponent,
    UserProfileComponent,
    NavComponent,
    UploadMediaComponent,
    SearchBarComponent,
    EmailConfirmationComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
