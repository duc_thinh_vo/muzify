--
-- Name: email_verification; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE email_verification (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  verifaction_code text,
  user_uuid varchar(36) NOT NULL,
  FOREIGN KEY(user_uuid) REFERENCES oauth_user(uuid) ON UPDATE CASCADE ON DELETE CASCADE
);
