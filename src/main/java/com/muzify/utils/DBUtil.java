/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author thinh
 */
public class DBUtil {
    private static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Muzify");
    
    public static EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
}
