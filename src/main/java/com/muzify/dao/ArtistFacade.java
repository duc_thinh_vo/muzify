/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.dao;

import com.muzify.domain.Artist;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author thinh
 */
@Stateless
public class ArtistFacade extends AbstractFacade<Artist> {

    @PersistenceContext(unitName = "com.mycompany_Muzify_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ArtistFacade() {
        super(Artist.class);
    }
    
    public List<Artist> searchArtistsByName(String name) {
        return em.createNamedQuery("Artist.searchByName").setParameter("name", "%"+name+"%").getResultList();
    }
}
