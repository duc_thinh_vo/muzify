/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

import com.muzify.domain.OauthUser;
import com.muzify.hk2factory.OauthUserFactory;
import javax.enterprise.context.RequestScoped;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

/**
 *
 * @author thinh
 */
public class OauthUserBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bindFactory(OauthUserFactory.class).to(OauthUser.class).in(RequestScoped.class);
    }
    
}
