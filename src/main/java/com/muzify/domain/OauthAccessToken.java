/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thinh
 */
@Entity
@Table(name = "oauth_access_token")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OauthAccessToken.findAll", query = "SELECT o FROM OauthAccessToken o")
    , @NamedQuery(name = "OauthAccessToken.findById", query = "SELECT o FROM OauthAccessToken o WHERE o.id = :id")
    , @NamedQuery(name = "OauthAccessToken.findByAccessToken", query = "SELECT o FROM OauthAccessToken o WHERE o.accessToken = :accessToken")
    , @NamedQuery(name = "OauthAccessToken.findByClientId", query = "SELECT o FROM OauthAccessToken o WHERE o.clientId = :clientId")
    , @NamedQuery(name = "OauthAccessToken.findByExpires", query = "SELECT o FROM OauthAccessToken o WHERE o.expires = :expires")
    , @NamedQuery(name = "OauthAccessToken.findByScope", query = "SELECT o FROM OauthAccessToken o WHERE o.scope = :scope")
    , @NamedQuery(name = "OauthAccessToken.selectLatestAccessTokenForUser", query = "SELECT o FROM OauthAccessToken o WHERE o.userUuid = :userUuid ORDER BY o.id DESC")})
public class OauthAccessToken implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "access_token")
    private String accessToken;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "client_id")
    private String clientId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "expires")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expires;
    @Size(max = 2000)
    @Column(name = "scope")
    private String scope;
    @JoinColumn(name = "user_uuid", referencedColumnName = "uuid")
    @ManyToOne(optional = false)
    private OauthUser userUuid;

    public OauthAccessToken() {
    }

    public OauthAccessToken(Long id) {
        this.id = id;
    }

    public OauthAccessToken(Long id, String accessToken, String clientId, Date expires) {
        this.id = id;
        this.accessToken = accessToken;
        this.clientId = clientId;
        this.expires = expires;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public OauthUser getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(OauthUser userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OauthAccessToken)) {
            return false;
        }
        OauthAccessToken other = (OauthAccessToken) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.muzify.domain.OauthAccessToken[ id=" + id + " ]";
    }
    
}
