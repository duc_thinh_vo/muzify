/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thinh
 */
@Entity
@Table(name = "user_profile_photo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserProfilePhoto.findAll", query = "SELECT u FROM UserProfilePhoto u")
    , @NamedQuery(name = "UserProfilePhoto.findById", query = "SELECT u FROM UserProfilePhoto u WHERE u.id = :id")
    , @NamedQuery(name = "UserProfilePhoto.findBySizeInfoWidth", query = "SELECT u FROM UserProfilePhoto u WHERE u.sizeInfoWidth = :sizeInfoWidth")
    , @NamedQuery(name = "UserProfilePhoto.findBySizeInfoHeight", query = "SELECT u FROM UserProfilePhoto u WHERE u.sizeInfoHeight = :sizeInfoHeight")})
public class UserProfilePhoto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "size_info_width")
    private Integer sizeInfoWidth;
    @Column(name = "size_info_height")
    private Integer sizeInfoHeight;
    @Lob
    @Size(max = 65535)
    @Column(name = "location_url")
    private String locationUrl;
    @JoinColumn(name = "user_uuid", referencedColumnName = "uuid")
    @ManyToOne
    private OauthUser userUuid;

    public UserProfilePhoto() {
    }

    public UserProfilePhoto(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSizeInfoWidth() {
        return sizeInfoWidth;
    }

    public void setSizeInfoWidth(Integer sizeInfoWidth) {
        this.sizeInfoWidth = sizeInfoWidth;
    }

    public Integer getSizeInfoHeight() {
        return sizeInfoHeight;
    }

    public void setSizeInfoHeight(Integer sizeInfoHeight) {
        this.sizeInfoHeight = sizeInfoHeight;
    }

    public String getLocationUrl() {
        return locationUrl;
    }

    public void setLocationUrl(String locationUrl) {
        this.locationUrl = locationUrl;
    }

    public OauthUser getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(OauthUser userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserProfilePhoto)) {
            return false;
        }
        UserProfilePhoto other = (UserProfilePhoto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.muzify.domain.UserProfilePhoto[ id=" + id + " ]";
    }
    
}
