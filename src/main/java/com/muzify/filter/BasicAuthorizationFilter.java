/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.filter;

import com.muzify.dao.OauthClientFacade;
import com.muzify.domain.OauthClient;
import com.muzify.exception.ErrorMessage;
import com.muzify.exception.NotAuthorizedException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author thinh
 */

public class BasicAuthorizationFilter implements ContainerRequestFilter {
    
    @EJB
    OauthClientFacade oauthClientFacade;
    
    @Override
    public void filter(ContainerRequestContext crc) throws IOException {
        String authorization = crc.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (authorization != null && authorization.startsWith("Basic")) {
            // Authorization: Basic base64credentials
            String base64Credentials = authorization.substring("Basic".length()).trim();
            String credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8"));
            // credentials = username:password
            final String[] values = credentials.split(":",2);
            if (values.length != 2) {
                ErrorMessage errorMessage = new ErrorMessage("", "Incorrect format for the Basic Authorization header.", "", Response.Status.UNAUTHORIZED);
                throw new NotAuthorizedException(errorMessage);
            }
            String id = values[0];
            String secret = values[1];
            
            // TODO: Refactor this. Create a base request filter.
            if (oauthClientFacade == null) {
                BeanManager beanManager = CDI.current().getBeanManager();
                Bean<?> bean = beanManager.getBeans(OauthClientFacade.class).iterator().next();
                CreationalContext<?> ctx = beanManager.createCreationalContext(bean);
                oauthClientFacade = (OauthClientFacade)beanManager.getReference(bean, OauthClientFacade.class, ctx);
            }
            
            if (oauthClientFacade != null) {
                OauthClient client = oauthClientFacade.findOauthClientByIdAndSecret(id, secret);
                if (client == null) {
                    ErrorMessage errorMessage = new ErrorMessage("", "Unauthorized. Incorrect Basic Authorization header", "", Response.Status.UNAUTHORIZED);
                    throw new NotAuthorizedException(errorMessage);
                }
                
                crc.setProperty("client", client);
            }
            
        } else {
            ErrorMessage errorMessage = new ErrorMessage("", "Missing Basic Authorization header.", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
    }
    
    
    
}
