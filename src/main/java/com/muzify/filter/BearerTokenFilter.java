/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.filter;

import com.muzify.dao.OauthAccessTokenFacade;
import com.muzify.dao.OauthRefreshTokenFacade;
import com.muzify.dao.OauthUserFacade;
import com.muzify.domain.OauthAccessToken;
import com.muzify.domain.OauthUser;
import com.muzify.exception.ErrorMessage;
import com.muzify.exception.NotAuthorizedException;
import com.muzify.hk2factory.OauthUserFactory;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author thinh
 */

@PreMatching
public class BearerTokenFilter implements ContainerRequestFilter {
    
    OauthUserFacade oauthUserFacade;
    
    OauthRefreshTokenFacade oauthRefreshTokenFacade;
    
    OauthAccessTokenFacade oauthAccessTokenFacade;
    
    private final String bearerString = "bearer ";
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException, NotAuthorizedException {
        
        String token = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        String parsedToken = parseToken(token);
        validateToken(parsedToken, requestContext);
    }
    
    private String parseToken(String bearerToken) throws NotAuthorizedException {
        
        if (bearerToken == null || bearerToken.trim().equals("")) {
            ErrorMessage errorMessage = new ErrorMessage("", "Missing AUTHORIZATION header.", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        if (!bearerToken.toLowerCase().startsWith(bearerString)) {
            
            ErrorMessage errorMessage = new ErrorMessage("", "Bearer token should start with \"Bearer \".", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        int startIndex = bearerString.length(); 
        String token = bearerToken.substring(startIndex);
        
        if (token == null || token.trim().length() <= 0) {
            ErrorMessage errorMessage = new ErrorMessage("", "Invalid bearer token.", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        return token.trim();
    }
    
    private void validateToken(String token, ContainerRequestContext requestContext) throws NotAuthorizedException {
        
        BeanManager beanManager = CDI.current().getBeanManager();
        Bean<?> bean = beanManager.getBeans(OauthUserFacade.class).iterator().next();
        CreationalContext<?> ctx = beanManager.createCreationalContext(bean);
        oauthUserFacade = (OauthUserFacade) beanManager.getReference(bean, OauthUserFacade.class, ctx);
        
        Bean<?> accessTokenBean = beanManager.getBeans(OauthAccessTokenFacade.class).iterator().next();
        CreationalContext<?> accessTokenContext = beanManager.createCreationalContext(accessTokenBean);
        oauthAccessTokenFacade = (OauthAccessTokenFacade) beanManager.getReference(accessTokenBean, OauthAccessTokenFacade.class, accessTokenContext);
        
        OauthAccessToken accessToken = oauthAccessTokenFacade.findAccessToken(token);
        if (accessToken == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Unable to authenticate by the given bearer token", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        if (accessToken.getExpires().before(new Date())) {
            ErrorMessage errorMessage = new ErrorMessage("", "Access token is already expired", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        if (accessToken.getUserUuid() == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Unable to authenticate by the given bearer token", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        requestContext.setProperty(OauthUserFactory.OauthUserKey, accessToken.getUserUuid());
    }
}
