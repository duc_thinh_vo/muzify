/*
 * Copyright 2017 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.muzify.resources;

import java.util.Set;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
/**
 *
 * @author thinh
 */
@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends javax.ws.rs.core.Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(com.wordnik.swagger.jaxrs.listing.ApiListingResource.class);
        resources.add(com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider.class);
        resources.add(com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON.class);
        resources.add(com.wordnik.swagger.jaxrs.listing.ResourceListingProvider.class);
        resources.add(MultiPartFeature.class);
        resources.add(OauthUserBinder.class);
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.muzify.dynamicfeature.AuthenticationFeature.class);
        resources.add(com.muzify.dynamicfeature.BasicAuthorizationFeature.class);
        resources.add(com.muzify.exception.ConflictException.class);
        resources.add(com.muzify.exception.NotAuthorizedException.class);
        resources.add(com.muzify.exception.ValidationExceptionMapper.class);
        resources.add(com.muzify.filter.CORSFilter.class);
        resources.add(com.muzify.resources.ArtistsResource.class);
        resources.add(com.muzify.resources.AudioResource.class);
        resources.add(com.muzify.resources.GenresResource.class);
        resources.add(com.muzify.resources.ImagesResource.class);
        resources.add(com.muzify.resources.PlaylistsResource.class);
        resources.add(com.muzify.resources.RegisterResource.class);
        resources.add(com.muzify.resources.SongsResource.class);
        resources.add(com.muzify.resources.UsersResource.class);
    }
}
