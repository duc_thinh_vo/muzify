/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author thinh
 */
@Embeddable
public class FollowPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "follower_id")
    private String followerId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 36)
    @Column(name = "followee_id")
    private String followeeId;

    public FollowPK() {
    }

    public FollowPK(String followerId, String followeeId) {
        this.followerId = followerId;
        this.followeeId = followeeId;
    }

    public String getFollowerId() {
        return followerId;
    }

    public void setFollowerId(String followerId) {
        this.followerId = followerId;
    }

    public String getFolloweeId() {
        return followeeId;
    }

    public void setFolloweeId(String followeeId) {
        this.followeeId = followeeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (followerId != null ? followerId.hashCode() : 0);
        hash += (followeeId != null ? followeeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FollowPK)) {
            return false;
        }
        FollowPK other = (FollowPK) object;
        if ((this.followerId == null && other.followerId != null) || (this.followerId != null && !this.followerId.equals(other.followerId))) {
            return false;
        }
        if ((this.followeeId == null && other.followeeId != null) || (this.followeeId != null && !this.followeeId.equals(other.followeeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.muzify.domain.FollowPK[ followerId=" + followerId + ", followeeId=" + followeeId + " ]";
    }
    
}
