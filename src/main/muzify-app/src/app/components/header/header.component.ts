import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserSession } from '../../UserSession';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public get isUserLoggedIn(): boolean {
    return UserSession.isLoggedIn;
  }

  constructor(public router: Router) { }

  ngOnInit() {
  }

  public onLogoClick() {
    this.router.navigate(['home']);
  }

  public onLoginClick() {
    this.router.navigate(['login']);
  }

  public onUserProfileClick() {
    this.router.navigate(['user-profile']);
  }

  public onUploadMediaClick() {
    this.router.navigate(['upload-media']);
  }
}
