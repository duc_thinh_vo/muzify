--
-- Name: oauth_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE oauth_user (
    uuid varchar(36) PRIMARY KEY DEFAULT '' ,
    username varchar(255),
    password varchar(2000),
    email varchar(255),
    url text,
    usertype ENUM ('admin', 'user', 'puppet', 'moderator') DEFAULT 'user' NOT NULL,
    created timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    total_following integer DEFAULT 0 NOT NULL,
    total_followers integer DEFAULT 0 NOT NULL,
    description text,
    fb_id bigint,
    fb_access_token text NOT NULL,
    full_name varchar(255) DEFAULT '',
    language character(2) DEFAULT 'EN',
    country_code character(2) DEFAULT NULL,
    age integer,
    gender character varying(20),
    city character varying(255),
    date_of_birth date,
    verified boolean DEFAULT false NOT NULL,
    discoverable boolean DEFAULT true NOT NULL,
    account_status ENUM ('active', 'deactivated', 'deleted') DEFAULT 'active' NOT NULL,
    ip text
);

--
-- Set UUID before insert
--
-- CREATE TRIGGER before_insert_user
--   BEFORE INSERT ON oauth_user
--   FOR EACH ROW
--   SET new.uuid = UUID();

--
-- Name: user_thumbnail; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE user_profile_photo (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  size_info_width int,
  size_info_height int,
  location_url text,
  user_uuid varchar(36),
  FOREIGN KEY(user_uuid) REFERENCES oauth_user(uuid)
);

--
-- Name: artist; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE artist (
  id int AUTO_INCREMENT PRIMARY KEY,
  name varchar(200) DEFAULT '' NOT NULL,
  date_of_birth date,
  description text
);

--
-- Name: artist_image; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE artist_image (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  url text,
  name varchar(300),
  created_at timestamp DEFAULT NOW() NOT NULL,
  artist_id int,
  FOREIGN KEY(artist_id) REFERENCES artist(id) ON UPDATE CASCADE
);

--
-- Name: genre; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE genre (
  id int AUTO_INCREMENT PRIMARY KEY,
  name varchar(255) DEFAULT '' NOT NULL
);

--
-- Name: album; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE album (
  id int AUTO_INCREMENT PRIMARY KEY,
  name varchar(200) DEFAULT '' NOT NULL,
  released_date timestamp DEFAULT NOW() NOT NULL,
  genre_id int,
  FOREIGN KEY(genre_id) REFERENCES genre(id)
);

--
-- Name: album_image; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE album_image (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  url text,
  name varchar(300),
  created_at timestamp DEFAULT NOW() NOT NULL,
  album_id int,
  FOREIGN KEY(album_id) REFERENCES album(id)
);

--
-- Name: song; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE song (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  name varchar(300) NOT NULL,
  length float,
  url text,
  genre_id int,
  album_id int,
  FOREIGN KEY(genre_id) REFERENCES genre(id),
  FOREIGN KEY(album_id) REFERENCES album(id)
);


--
-- Name: lyric; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE lyric (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  song_id bigint,
  by_user_id varchar(36),
  lyric_text text,
  FOREIGN KEY(song_id)
    REFERENCES song(id),
  FOREIGN KEY(by_user_id)
    REFERENCES oauth_user(uuid)
);

--
-- Name: album_image; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE song_image (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  url text,
  name varchar(300),
  created_at timestamp DEFAULT NOW() NOT NULL,
  song_id bigint,
  FOREIGN KEY(song_id) REFERENCES song(id)
);

--
-- Name: song_owner; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE song_owner (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  user_uuid varchar(36) NOT NULL,
  song_id bigint NOT NULL,
  FOREIGN KEY(user_uuid) REFERENCES oauth_user(uuid),
  FOREIGN KEY(song_id) REFERENCES song(id)
);

--
-- Name: sung_by; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE sung_by (
  id int AUTO_INCREMENT PRIMARY KEY,
  song_id bigint,
  artist_id int,
  FOREIGN KEY(song_id) REFERENCES song(id),
  FOREIGN KEY (artist_id) REFERENCES artist(id)
);

CREATE TABLE playlist (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  user_uuid varchar(36),
  name varchar(200),
  FOREIGN KEY(user_uuid)
    REFERENCES oauth_user(uuid)
);

CREATE TABLE playlist_song (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  playlist_id bigint,
  song_id bigint,
  FOREIGN KEY(playlist_id) REFERENCES playlist(id),
  FOREIGN KEY(song_id) REFERENCES song(id)
);

CREATE TABLE post (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  title varchar(255) DEFAULT '',
  description text,
  creator_uuid varchar(36),
  created_at timestamp DEFAULT NOW() NOT NULL,
  song_id bigint,
  FOREIGN KEY(song_id)
    REFERENCES song(id),
  FOREIGN KEY(creator_uuid)
    REFERENCES oauth_user(uuid)
);

CREATE TABLE comment (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  comment_text text NOT NULL,
  post_id bigint NOT NULL,
  user_uuid varchar(36) NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  FOREIGN KEY(post_id) REFERENCES post(id),
  FOREIGN KEY(user_uuid) REFERENCES oauth_user(uuid)
);

CREATE TABLE follow (
  follower_id varchar(36) NOT NULL,
  followee_id varchar(36) NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  FOREIGN KEY(follower_id) REFERENCES oauth_user(uuid),
  FOREIGN KEY(followee_id) REFERENCES oauth_user(uuid),
  PRIMARY KEY(follower_id, followee_id)
);

CREATE TABLE like_to_post (
  user_uuid varchar(36) NOT NULL,
  post_id bigint NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  FOREIGN KEY(user_uuid) REFERENCES oauth_user(uuid),
  FOREIGN KEY(post_id) REFERENCES post(id),
  PRIMARY KEY(user_uuid, post_id)
);

--
--
--
INSERT INTO genre (name) VALUES
('Classical'),
('Blues'),
('Children'),
('Country'),
('Folk'),
('Holiday'),
('International'),
('Jazz'),
('Latin'),
('Rock'),
('Rap'),
('Reggae');

--
--
--
CREATE TABLE oauth_access_token (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  access_token varchar(40) NOT NULL,
  client_id varchar(80) NOT NULL,
  user_uuid varchar(36) NOT NULL,
  expires timestamp,
  scope varchar(2000),
  FOREIGN KEY (user_uuid) REFERENCES oauth_user(uuid)
);

--
--
--
CREATE TABLE oauth_refresh_token (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  refresh_token varchar(40) NOT NULL,
  client_id varchar(80) NOT NULL,
  user_uuid varchar(36) NOT NULL,
  expires timestamp,
  scope varchar(2000),
  FOREIGN KEY (user_uuid) REFERENCES oauth_user(uuid)
);

--
--
--
CREATE TABLE oauth_authorization_codes (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  authorization_code character varying(40) NOT NULL,
  client_id varchar(80) NOT NULL,
  user_uuid varchar(255),
  redirect_uri varchar(2000),
  expires timestamp,
  scope character varying(2000),
  FOREIGN KEY (user_uuid) REFERENCES oauth_user(uuid)
);


--
-- TOC entry 244 (class 1259 OID 21509)
-- Name: oauth_client; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE oauth_client (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  client_id varchar(80) NOT NULL,
  client_secret varchar(80) NOT NULL,
  redirect_uri varchar(2000) NOT NULL,
  grant_types varchar(80),
  scope varchar(100),
  user_uuid varchar(80)
);


INSERT INTO oauth_user (uuid, username, email, fb_access_token, fb_id, age, gender) VALUES ('ff8080816029fc75016029fc77ec0000', 'thinhvo', 'ducthinh2410@gmail.com', 'sdhfgkj5j6dsfgadf', 353647456745645, 22, 'M');
INSERT INTO artist(name) VALUES ('ThinhVo');
INSERT INTO artist_image(url, name, artist_id) VALUES ('https://i.ytimg.com/vi/xUGePDg4B1A/maxresdefault.jpg', 'cat', 1);
