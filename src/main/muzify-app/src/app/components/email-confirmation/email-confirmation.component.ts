import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.scss']
})

export class EmailConfirmationComponent implements OnInit {
  activationCode: string;
  confirmationCode: number = 0;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    // this.route.queryParams.subscribe(params => {
      // this.activationCode = params['activationCode'];
      this.activationCode = this.route.snapshot.queryParamMap.get('activationCode');
    // })
  }

}
