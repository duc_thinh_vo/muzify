/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.exception;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author thinh
 */

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<javax.validation.ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        String message = "";
        for (ConstraintViolation<?> violation : violations) {
            message += e.getMessage() + ". ";
        }
        
        ErrorMessage errorMessage = new ErrorMessage("", "" + e.getMessage(), "", Response.Status.BAD_REQUEST);
        return Response.status(Response.Status.BAD_REQUEST).entity(errorMessage.toJSON()).build();
    }
    
}
