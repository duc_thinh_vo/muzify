/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

/**
 *
 * @author thinh
 */
import com.muzify.dao.ArtistFacade;
import com.muzify.dao.ArtistImageFacade;
import com.muzify.domain.Artist;
import com.muzify.domain.ArtistImage;
import com.muzify.exception.ErrorMessage;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.muzify.exception.NotFoundException;
import com.muzify.utils.ImageFileManager;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Stateless
@Path("/artists")
@Api(value = "/artists", description="Artist endpoints")
public class ArtistsResource {
    
    @Context
    UriInfo uri;
    
    @EJB 
    ArtistFacade artistFacade;
    
    @EJB
    ArtistImageFacade artistImageFacde;
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Gets a specific artist", notes = "Fetches an artist by id")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 401, message = "Unauthorized request"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response getArtist(@NotNull @PathParam("id") Integer id) throws NotFoundException {
        
        Artist artist = artistFacade.find(id);
        if (artist == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Unable to find artist by the given id", "", Response.Status.NOT_FOUND);
            throw new NotFoundException(errorMessage);
        }
        
        return Response.status(200).entity(artist).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Gets a specific artist", notes = "Fetches an artist by id")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 401, message = "Unauthorized request"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response getAllArtists(@QueryParam("name" ) String name) throws NotFoundException {
        
        List<Artist> artists = new ArrayList<Artist>();
        if (name == null || name.trim().equals("")) {
            artists = artistFacade.findAll();
        } else {
            artists = artistFacade.searchArtistsByName(name);
        }
        
        GenericEntity<List<Artist>> genericEntity = new GenericEntity<List<Artist>>(artists){};
        return Response.status(200).entity(genericEntity).build();
    }
    
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Creates an artist", notes = "Creating an artist requires admin privileges")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code =  401, message = "Unauthorized request"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response post(@FormDataParam("name") String name,
                         @FormDataParam("file") InputStream inputStream,
                         @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
        Artist artist = new Artist();
        artist.setName(name);
        
        /**
         * TODO: Move image handling code to ArtistService
         */
        // Image Upload
        if (inputStream != null && fileDetail != null) {
            List<ArtistImage> images = new ArrayList<ArtistImage>();
            
            ImageFileManager ifm = new ImageFileManager();
            
            String fileName = fileDetail.getFileName();
            String fileExtension = ifm.getFileExtension(fileName);
            
            // Generate random name
            String randomFileName = UUID.randomUUID().toString() + "." + fileExtension;
            
            // Save image to disk
            ifm.writeFileToDisk(inputStream, randomFileName, true);
            
            // Create URL for image
            String imageURI = uri.getBaseUri().toString() + "images/" + randomFileName;
            
            // Create image
            ArtistImage image = new ArtistImage();
            image.setName(randomFileName);
            image.setUrl(imageURI);
            image.setArtistId(artist);

            images.add(image);
            artist.setArtistImageCollection(images);
            
        }
        
        artistFacade.create(artist);
        
        return Response.status(200).entity(artist).build();
    }
    
    @DELETE
    @Path("{id}")
    @ApiOperation(value = "Deletes a specific artist", notes = "Deletes an artist by id")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Deleted successfully"),
        @ApiResponse(code = 401, message = "Unauthorized request"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response delete(@PathParam("id") Integer id) {
        //TODO: Only admin is able to delete image
        Artist artist = artistFacade.find(id);
        if (artist == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Not Found", "", Response.Status.BAD_REQUEST);
            throw new NotFoundException(errorMessage);
        }
        artistFacade.remove(artist);
        return Response.status(204).entity(null).build();
    }
}
