/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.dao;

import com.muzify.domain.EmailVerification;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author thinh
 */
@Stateless
public class EmailVerificationFacade extends AbstractFacade<EmailVerification> {

    @PersistenceContext(unitName = "com.mycompany_Muzify_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmailVerificationFacade() {
        super(EmailVerification.class);
    }
    
    public EmailVerification findEmailVerificationByCode(String verificationCode) {
        try {
            return (EmailVerification)em.createNamedQuery("EmailVerification.findByCode").setParameter("code", verificationCode).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
