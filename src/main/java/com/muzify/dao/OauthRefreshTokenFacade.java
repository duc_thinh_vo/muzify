/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.dao;

import com.muzify.domain.OauthRefreshToken;
import com.muzify.domain.OauthUser;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author thinh
 */
@Stateless
public class OauthRefreshTokenFacade extends AbstractFacade<OauthRefreshToken> {

    @PersistenceContext(unitName = "com.mycompany_Muzify_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OauthRefreshTokenFacade() {
        super(OauthRefreshToken.class);
    }
    
    public OauthRefreshToken findRefeshTokenForUserAndClientId(OauthUser user, String clientId) {
        try {
            OauthRefreshToken refreshToken = (OauthRefreshToken)em.createNamedQuery("OauthRefreshToken.findByUserAndClientId").setParameter("userUuid", user).setParameter("clientId", clientId).setMaxResults(1).getSingleResult();
            return refreshToken;
        } catch (NoResultException e) {
            return null;
        }
    }
    
}
