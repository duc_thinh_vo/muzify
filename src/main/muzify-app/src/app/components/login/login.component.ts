import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from '../../services/user-data/user-data.service';
import {UserSession} from '../../UserSession';

declare var FB: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(public router: Router,
              private userDataService: UserDataService) { }

  ngOnInit() {
  }

  public onLoginClick() {
    FB.getLoginStatus((response) => {
      if (response.status === 'connected') {
        console.log(response.authResponse.accessToken);
        this.userDataService.loginViaFacebook(response.authResponse.accessToken).subscribe(serverResponse => {
          UserSession.loginSuccessfullyWithDictionary(serverResponse);
          console.log(serverResponse);
        });
        this.router.navigate(['home']);
      } else {
        FB.login((loginResponse) => {
          console.log(response.authResponse.accessToken);
          this.userDataService.loginViaFacebook(loginResponse.authResponse.accessToken).subscribe(serverResponse => {
            UserSession.loginSuccessfullyWithDictionary(serverResponse);
          });
          this.router.navigate(['home']);
        });
      }
    });
  }
}
