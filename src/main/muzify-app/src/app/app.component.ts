import { Component } from '@angular/core';
import {UserSession} from './UserSession';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  // emailConfirmation: boolean;

  constructor(public router: Router) {
  }

  public get isUserLoggedIn(): boolean {
    return UserSession.isLoggedIn;
  }

  public get emailConfirmation(): boolean {
    if (this.router.url === '/email-confirmation')
      return true;
  }
}
