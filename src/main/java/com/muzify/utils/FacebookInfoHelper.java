/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muzify.dao.OauthUserFacade;
import com.muzify.domain.OauthUser;
import com.muzify.exception.ErrorMessage;
import com.muzify.exception.NotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

/**
 *
 * @author thinh
 */

public class FacebookInfoHelper {

    private static final String facebookResourceURL = "https://graph.facebook.com/me?fields=id,name,birthday,email";
    
    public static OauthUser oauthUserForAccessToken(String accessToken) throws NotFoundException, OAuthSystemException, OAuthProblemException, IOException {
        
        OAuthClient oauthClient = new OAuthClient(new URLConnectionClient());
        OAuthClientRequest request = new OAuthBearerClientRequest(facebookResourceURL).setAccessToken(accessToken).buildQueryMessage();
        OAuthResourceResponse response = oauthClient.resource(request, OAuth.HttpMethod.GET, OAuthResourceResponse.class);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(response.getBody());
        
        JsonNode facebookIdValue = jsonNode.get("id");
        JsonNode emailValue = jsonNode.get("email");
        JsonNode nameValue = jsonNode.get("name");
        
        if (emailValue == null || facebookIdValue.equals("") || facebookIdValue == null || facebookIdValue.equals("") || nameValue == null || nameValue.equals("")) {
            ErrorMessage errorMessage = new ErrorMessage("", "Could not find any Facebook user with by the given access token", "", Response.Status.NOT_FOUND);
            throw new NotFoundException(errorMessage);
        }

                
        OauthUser user = new OauthUser();
        user.setEmail(emailValue.asText());
        user.setFullName(nameValue.asText());
        user.setFbId(new BigInteger(facebookIdValue.asText()));
        user.setFbAccessToken(accessToken);
        
        return user;
    }
}
