/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thinh
 */
@Entity
@Table(name = "oauth_client")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OauthClient.findAll", query = "SELECT o FROM OauthClient o")
    , @NamedQuery(name = "OauthClient.findById", query = "SELECT o FROM OauthClient o WHERE o.id = :id")
    , @NamedQuery(name = "OauthClient.findByClientId", query = "SELECT o FROM OauthClient o WHERE o.clientId = :clientId")
    , @NamedQuery(name = "OauthClient.findByClientSecret", query = "SELECT o FROM OauthClient o WHERE o.clientSecret = :clientSecret")
    , @NamedQuery(name = "OauthClient.findByClientIdAndSecret", query = "SELECT o FROM OauthClient o WHERE o.clientId = :clientId AND o.clientSecret = :clientSecret")
    , @NamedQuery(name = "OauthClient.findByRedirectUri", query = "SELECT o FROM OauthClient o WHERE o.redirectUri = :redirectUri")
    , @NamedQuery(name = "OauthClient.findByGrantTypes", query = "SELECT o FROM OauthClient o WHERE o.grantTypes = :grantTypes")
    , @NamedQuery(name = "OauthClient.findByScope", query = "SELECT o FROM OauthClient o WHERE o.scope = :scope")
    , @NamedQuery(name = "OauthClient.findByUserUuid", query = "SELECT o FROM OauthClient o WHERE o.userUuid = :userUuid")})
public class OauthClient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "client_id")
    private String clientId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "client_secret")
    private String clientSecret;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "redirect_uri")
    private String redirectUri;
    @Size(max = 80)
    @Column(name = "grant_types")
    private String grantTypes;
    @Size(max = 100)
    @Column(name = "scope")
    private String scope;
    @Size(max = 80)
    @Column(name = "user_uuid")
    private String userUuid;

    public OauthClient() {
    }

    public OauthClient(Long id) {
        this.id = id;
    }

    public OauthClient(Long id, String clientId, String clientSecret, String redirectUri) {
        this.id = id;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.redirectUri = redirectUri;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getGrantTypes() {
        return grantTypes;
    }

    public void setGrantTypes(String grantTypes) {
        this.grantTypes = grantTypes;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OauthClient)) {
            return false;
        }
        OauthClient other = (OauthClient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.muzify.domain.OauthClient[ id=" + id + " ]";
    }
    
}
