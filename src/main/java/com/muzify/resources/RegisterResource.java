/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

import com.muzify.dao.EmailVerificationFacade;
import com.muzify.dao.OauthAccessTokenFacade;
import com.muzify.dao.OauthRefreshTokenFacade;
import com.muzify.dao.OauthUserFacade;
import com.muzify.domain.EmailVerification;
import com.muzify.domain.OauthAccessToken;
import com.muzify.domain.OauthClient;
import com.muzify.domain.OauthRefreshToken;
import com.muzify.domain.OauthTokenResponse;
import com.muzify.domain.OauthUser;
import com.muzify.dynamicfeature.ClientAuthenticated;
import com.muzify.exception.ConflictException;
import com.muzify.exception.ErrorMessage;
import com.muzify.exception.NotFoundException;
import com.muzify.utils.DateUtil;
import com.muzify.utils.MailManager;
import com.muzify.utils.StringHelper;
import java.util.Date;
import javax.ejb.EJB;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.mindrot.jbcrypt.BCrypt;

/**
 * REST Web Service
 *
 * @author thinh
 */
@Path("register")
public class RegisterResource {

    @Context
    private UriInfo context;

    @EJB
    OauthUserFacade oauthUserFace;
    
    @EJB
    OauthRefreshTokenFacade refreshTokenFacade;
    
    @EJB
    OauthAccessTokenFacade accessTokenFacade;
    
    @EJB
    EmailVerificationFacade emailVerificationFacade; 
    /**
     * Creates a new instance of RegisterResource
     */
    public RegisterResource() {
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ClientAuthenticated
    public Response register(
        @FormParam("email") 
        @NotNull(message = "This email must not be null")
        @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
        +"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*"
        +"@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
             message="{invalid.email}")
        String email,
        @FormParam("username")
        @NotNull(message = "This username must not be null")
        @Size(min=6, max=240)
        String username,
        @FormParam("password")
        @NotNull(message = "This username must not be null")
        @Size(min=6, max=240)
        String password,
        @Context ContainerRequestContext requestContext) throws OAuthSystemException {

        OauthUser user = oauthUserFace.findUserByEmail(email);
        if (user != null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Email is already existed", "", Response.Status.CONFLICT);
            throw new ConflictException(errorMessage);
        }
        
        String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
        user = new OauthUser();
        user.setEmail(email);
        user.setPassword(hashed);
        user.setUsername(username);
        user.setFbAccessToken("unknown");
        
        oauthUserFace.create(user);
        
        OauthClient client = (OauthClient)requestContext.getProperty("client");
        String clientId = client.getClientId();
        
        OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
        
        // Generate access token
        OauthAccessToken generatedAccessToken = new OauthAccessToken();
        generatedAccessToken.setClientId(clientId);
        generatedAccessToken.setAccessToken(oauthIssuerImpl.accessToken());
        generatedAccessToken.setUserUuid(user);
        generatedAccessToken.setScope("all");
        generatedAccessToken.setExpires(DateUtil.addHours(new Date(), 1));
        accessTokenFacade.create(generatedAccessToken);
        
        // RefreshToken
        OauthRefreshToken refreshToken = refreshTokenFacade.findRefeshTokenForUserAndClientId(user, clientId);
        if (refreshToken == null) {
            refreshToken = new OauthRefreshToken();
            refreshToken.setClientId(clientId);
            refreshToken.setUserUuid(user);
            refreshToken.setScope("all");
            refreshToken.setRefreshToken(oauthIssuerImpl.refreshToken());
            refreshTokenFacade.create(refreshToken);
        }
        
        OauthTokenResponse response = new OauthTokenResponse();
        response.setAccessToken(generatedAccessToken.getAccessToken());
        response.setExpire(3600);
        response.setScope(generatedAccessToken.getScope());
        response.setUser(user);
        response.setTokenType("Bearer");
        response.setRefreshToken(refreshToken.getRefreshToken());
        
        String verificationCode = StringHelper.randomAlphaNumeric(255);
        EmailVerification emailVerification = new EmailVerification();
        emailVerification.setUserUuid(user);
        emailVerification.setVerifactionCode(verificationCode);
        emailVerificationFacade.create(emailVerification);
        
        String verificationURL = "https://muzify.eu/#/email-confirmation?activationCode=" + verificationCode;
        MailManager.sendVerificationEmail(email, verificationURL);
        
        
        return Response.status(Response.Status.CREATED).entity(response).build();
    }
    
    @Path("verify")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ClientAuthenticated
    public Response verify(
            @NotNull(message = "This username must not be null")
            @FormParam("verificationCode") String verificationCode) {
        
        EmailVerification emailVerification = emailVerificationFacade.findEmailVerificationByCode(verificationCode);
        
        if (emailVerification == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Email verification code is not found", "", Response.Status.NOT_FOUND);
            throw new NotFoundException(errorMessage);
        } else {
            OauthUser user = emailVerification.getUserUuid();
            user.setVerified(true);
            oauthUserFace.edit(user);
        }
        
        return Response.status(Response.Status.CREATED).entity(verificationCode).build();
    }
}
