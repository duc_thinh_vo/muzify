/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.utils;

import javax.mail.Authenticator;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author thinh
 */
public class MailManager {
    
    public static void sendVerificationEmail(String email, String verificationURL) {
        String from = "@gmail.com";
        final String username = "@gmail.com";
        final String password = "";
        
        String host = "smtp.gmail.com";
        
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");
        
        Authenticator auth =  new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };
        Session session = Session.getInstance(props, auth);
        try {
         // Create a default MimeMessage object.
         Message message = new MimeMessage(session);
         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.setRecipients(Message.RecipientType.TO,
         InternetAddress.parse(email));

         // Set Subject: header field
         message.setSubject("Muzify Verification");

         // Now set the actual message
         String content = MailManager.verificationEmail(email, verificationURL);
         message.setContent(content, "text/html; charset=utf-8");

         // Send message
         Transport.send(message);

         System.out.println("Sent message successfully....");

      } catch (MessagingException e) {
            throw new RuntimeException(e);
      }
    }
    
    public static String verificationEmail(String email, String verificationURL) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "  \n" +
                "  <head>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n" +
                "    <title>Revue</title>\n" +
                "    <style type=\"text/css\">\n" +
                "      #outlook a {padding:0;}\n" +
                "      body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;} \n" +
                "      .ExternalClass {width:100%;}\n" +
                "      .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div, .ExternalClass blockquote {line-height: 100%;}\n" +
                "      .ExternalClass p, .ExternalClass blockquote {margin-bottom: 0; margin: 0;}\n" +
                "      #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}\n" +
                "      \n" +
                "      img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} \n" +
                "      a img {border:none;} \n" +
                "      .image_fix {display:block;}\n" +
                "  \n" +
                "      p {margin: 1em 0;}\n" +
                "  \n" +
                "      h1, h2, h3, h4, h5, h6 {color: black !important;}\n" +
                "      h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: black;}\n" +
                "      h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {color: black;}\n" +
                "      h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {color: black;}\n" +
                "  \n" +
                "      table td {border-collapse: collapse;}\n" +
                "      table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }\n" +
                "  \n" +
                "      a {color: #3498db;}\n" +
                "      p.domain a{color: black;}\n" +
                "  \n" +
                "      hr {border: 0; background-color: #d8d8d8; margin: 0; margin-bottom: 0; height: 1px;}\n" +
                "  \n" +
                "      @media (max-device-width: 667px) {\n" +
                "        a[href^=\"tel\"], a[href^=\"sms\"] {\n" +
                "          text-decoration: none;\n" +
                "          color: blue;\n" +
                "          pointer-events: none;\n" +
                "          cursor: default;\n" +
                "        }\n" +
                "  \n" +
                "        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n" +
                "          text-decoration: default;\n" +
                "          color: orange !important;\n" +
                "          pointer-events: auto;\n" +
                "          cursor: default;\n" +
                "        }\n" +
                "  \n" +
                "        h1[class=\"profile-name\"], h1[class=\"profile-name\"] a {\n" +
                "          font-size: 32px !important;\n" +
                "          line-height: 38px !important;\n" +
                "          margin-bottom: 14px !important;\n" +
                "        }\n" +
                "  \n" +
                "        span[class=\"issue-date\"], span[class=\"issue-date\"] a {\n" +
                "          font-size: 14px !important;\n" +
                "          line-height: 22px !important;\n" +
                "        }\n" +
                "  \n" +
                "        td[class=\"description-before\"] {\n" +
                "          padding-bottom: 28px !important;\n" +
                "        }\n" +
                "        td[class=\"description\"] {\n" +
                "          padding-bottom: 14px !important;\n" +
                "        }\n" +
                "        td[class=\"description\"] span, span[class=\"item-text\"], span[class=\"item-text\"] span {\n" +
                "          font-size: 16px !important;\n" +
                "          line-height: 24px !important;\n" +
                "        }\n" +
                "  \n" +
                "        span[class=\"item-link-title\"] {\n" +
                "          font-size: 18px !important;\n" +
                "          line-height: 24px !important;\n" +
                "        }\n" +
                "  \n" +
                "        span[class=\"item-header\"] {\n" +
                "          font-size: 22px !important;\n" +
                "        }\n" +
                "  \n" +
                "        span[class=\"item-link-description\"], span[class=\"item-link-description\"] span {\n" +
                "          font-size: 14px !important;\n" +
                "          line-height: 22px !important;\n" +
                "        }\n" +
                "  \n" +
                "        .link-image {\n" +
                "          width: 84px !important;\n" +
                "          height: 84px !important;\n" +
                "        }\n" +
                "  \n" +
                "        .link-image img {\n" +
                "          max-width: 100% !important;\n" +
                "          max-height: 100% !important;\n" +
                "        }\n" +
                "  \n" +
                "      }\n" +
                "  \n" +
                "      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n" +
                "        a[href^=\"tel\"], a[href^=\"sms\"] {\n" +
                "          text-decoration: none;\n" +
                "          color: blue;\n" +
                "          pointer-events: none;\n" +
                "          cursor: default;\n" +
                "        }\n" +
                "  \n" +
                "        .mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {\n" +
                "          text-decoration: default;\n" +
                "          color: orange !important;\n" +
                "          pointer-events: auto;\n" +
                "          cursor: default;\n" +
                "        }\n" +
                "      }\n" +
                "    </style>\n" +
                "    <!--[if gte mso 9]>\n" +
                "      <style type=\"text/css\">\n" +
                "        #contentTable {\n" +
                "          width: 600px;\n" +
                "        }\n" +
                "      </style>\n" +
                "    <![endif]-->\n" +
                "  </head>\n" +
                "  \n" +
                "  <body style=\"width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;\">\n" +
                "    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" style=\"margin:0; padding:0; width:100% !important; line-height: 100% !important; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;\"\n" +
                "    width=\"100%\">\n" +
                "      <tr>\n" +
                "        <td width=\"10\" valign=\"top\">&nbsp;</td>\n" +
                "        <td valign=\"top\" align=\"center\">\n" +
                "          <!--[if (gte mso 9)|(IE)]>\n" +
                "            <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background-color: #FFF; border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;\">\n" +
                "              <tr>\n" +
                "                <td>\n" +
                "                <![endif]-->\n" +
                "                <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" style=\"width: 100%; max-width: 600px; background-color: #FFF; border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;\"\n" +
                "                id=\"contentTable\">\n" +
                "                  <tr>\n" +
                "                    <td width=\"600\" valign=\"top\" align=\"center\" style=\"border-collapse:collapse;\">\n" +
                "                      <table align='center' border='0' cellpadding='0' cellspacing='0' style='border: 1px solid #E0E4E8;'\n" +
                "                      width='100%'>\n" +
                "                        <tr>\n" +
                "                          <td align='left' style='padding: 56px 56px 28px 56px;' valign='top'>\n" +
                "                            <div style='font-family: \"lato\", \"Helvetica Neue\", Helvetica, Arial, sans-serif; line-height: 28px;font-size: 18px; color: #333;font-weight:bold;'>Hey there!</div>\n" +
                "                          </td>\n" +
                "                        </tr>\n" +
                "                        <tr>\n" +
                "                          <td align='left' style='padding: 0 56px 28px 56px;' valign='top'>\n" +
                "                            <div style='font-family: \"lato\", \"Helvetica Neue\", Helvetica, Arial, sans-serif; line-height: 28px;font-size: 18px; color: #333;'>Please click the following link to confirm that <strong>"+ email +"</strong> is\n" +
                "                              your email address where you will receive replies to your issues:</div>\n" +
                "                          </td>\n" +
                "                        </tr>\n" +
                "                        <tr>\n" +
                "                          <td align='left' style='padding: 0 56px;' valign='top'>\n" +
                "                            <div>\n" +
                "                              <!--[if mso]>\n" +
                "                                <v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\"\n" +
                "                                href=\"#\"\n" +
                "                                style=\"height:44px;v-text-anchor:middle;width:250px;\" arcsize=\"114%\" stroke=\"f\"\n" +
                "                                fillcolor=\"#E15718\">\n" +
                "                                  <w:anchorlock/>\n" +
                "                                <![endif]-->\n" +
                "                                <a style=\"background-color:#E15718;border-radius:50px;color:#ffffff;display:inline-block;font-family: &#39;lato&#39;, &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif;font-size:18px;line-height:44px;text-align:center;text-decoration:none;width:250px;-webkit-text-size-adjust:none;\"\n" +
                "                                href=\""+ verificationURL +"\">Confirm your email</a>\n" +
                "                                <!--[if mso]>\n" +
                "                                </v:roundrect>\n" +
                "                              <![endif]-->\n" +
                "                            </div>\n" +
                "                          </td>\n" +
                "                          <tr>\n" +
                "                            <td align='left' style='padding: 28px 56px 28px 56px;' valign='top'></td>\n" +
                "                          </tr>\n" +
                "                        </tr>\n" +
                "                      </table>\n" +
                "                      <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>\n" +
                "                        <tr>\n" +
                "                          <td align='center' style='padding: 30px 56px 28px 56px;' valign='middle'>\n" +
                "<span style='font-family: \"lato\", \"Helvetica Neue\", Helvetica, Arial, sans-serif; line-height: 28px;font-size: 16px; color: #A7ADB5; vertical-align: middle;'>If this email doesn't make any sense, please <a href=\"mailto:support@getrevue.co\">let us know</a>!</span>\n" +
                "\n" +
                "                          </td>\n" +
                "                        </tr>\n" +
                "                        <tr>\n" +
                "                          <td align='center' style='padding: 0 56px 28px 56px;' valign='middle'>\n" +
                "                            <a style=\"border: 0;\" href=\"https://www.getrevue.co/?utm_campaign=Reset+password&utm_content=logo&utm_medium=email&utm_source=reset_password\">\n" +
                "                              <img alt=\"Revue\" width=\"70\" height=\"28\" style=\"vertical-align: middle;\" src=\"https://www.getrevue.co/assets/email/revue_gray-99f4cab9d76e074438c58f6055605e4d45773b72dc55dd8b8305b0634fc4f381.png\"\n" +
                "                              />\n" +
                "                            </a>\n" +
                "                          </td>\n" +
                "                        </tr>\n" +
                "                      </table>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </table>\n" +
                "                <!--[if (gte mso 9)|(IE)]>\n" +
                "                </td>\n" +
                "              </tr>\n" +
                "            </table>\n" +
                "          <![endif]-->\n" +
                "        </td>\n" +
                "        <td width=\"10\" valign=\"top\">&nbsp;</td>\n" +
                "      </tr>\n" +
                "    </table>\n" +
                "    \n" +
                "  </body>\n" +
                "\n" +
                "</html>";
    }
}
