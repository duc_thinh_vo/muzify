/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.dao;

import com.muzify.domain.OauthUser;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author thinh
 */
@Stateless
public class OauthUserFacade extends AbstractFacade<OauthUser> {

    @PersistenceContext(unitName = "com.mycompany_Muzify_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OauthUserFacade() {
        super(OauthUser.class);
    }
    
    public OauthUser findUserByEmail(String email) {
        try {
            return (OauthUser)em.createNamedQuery("OauthUser.findByEmail").setParameter("email", email).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        
    }
    
}
