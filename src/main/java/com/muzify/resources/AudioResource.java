/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

import com.muzify.exception.ErrorMessage;
import com.muzify.exception.NotFoundException;
import com.muzify.utils.AudioFileManager;
import com.muzify.utils.MediaStreamer;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

/**
 * REST Web Service
 *
 * @author thinh
 */
@Stateless
@Path("audio")
@Api(value = "/audio", description="Serves audio")
public class AudioResource {
    final int chunk_size = 1024 * 1024;
    
    @Context
    private UriInfo context;
    
    @GET
    @Path("{filename}")
    @Produces({"audio/basic  ", "audio/mpeg", "audio/vnd.wav"})
    @ApiOperation(value = "Returns an audio file", notes = "Returns an audio file")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "Not found"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response get(@PathParam("filename") String fileName, @HeaderParam("Range") String range) throws NotFoundException, IOException, Exception {
        AudioFileManager afm = new AudioFileManager();
        File audioFile = new File(afm.storageFilePath() + fileName); // TODO: Remove this
        
        if (fileName == null || fileName.equals("") || !audioFile.exists()) {
            ErrorMessage message = new ErrorMessage("", "The requested audio is not found", "", Response.Status.NOT_FOUND);
            throw new NotFoundException(message);
        }
        /*
        final InputStream inputStream = new FileInputStream(afm.storageFilePath() + fileName);
        
        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                int length;
                byte[] buffer = new byte[1024];
                while((length = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, length);
                }
          
                output.flush();
                inputStream.close();
            }
          }; */
        
        return buildStream(audioFile, range);
        //return Response.ok(stream).header("Content-Disposition", "attachment; filename=\"" + fileName +"\"").build();
    }
    
    private Response buildStream(final File asset, final String range) throws Exception {
        // range not requested : Firefox, Opera, IE do not send range headers
        if (range == null) {
            StreamingOutput streamer = new StreamingOutput() {
                @Override
                public void write(final OutputStream output) throws IOException, WebApplicationException {

                    final FileChannel inputChannel = new FileInputStream(asset).getChannel();
                    final WritableByteChannel outputChannel = Channels.newChannel(output);
                    try {
                        inputChannel.transferTo(0, inputChannel.size(), outputChannel);
                    } finally {
                        // closing the channels
                        inputChannel.close();
                        outputChannel.close();
                    }
                }
            };
            return Response.ok(streamer).header(HttpHeaders.CONTENT_LENGTH, asset.length()).build();
        }

        String[] ranges = range.split("=")[1].split("-");
        final int from = Integer.parseInt(ranges[0]);
        /**
         * Chunk media if the range upper bound is unspecified. Chrome sends "bytes=0-"
         */
        int to = chunk_size + from;
        if (to >= asset.length()) {
            to = (int) (asset.length() - 1);
        }
        if (ranges.length == 2) {
            to = Integer.parseInt(ranges[1]);
        }

        final String responseRange = String.format("bytes %d-%d/%d", from, to, asset.length());
        final RandomAccessFile raf = new RandomAccessFile(asset, "r");
        raf.seek(from);

        final int len = to - from + 1;
        final MediaStreamer streamer = new MediaStreamer(len, raf);
        Response.ResponseBuilder res = Response.status(Status.PARTIAL_CONTENT).entity(streamer)
                .header("Accept-Ranges", "bytes")
                .header("Content-Range", responseRange)
                .header(HttpHeaders.CONTENT_LENGTH, streamer.getLenth())
                .header(HttpHeaders.LAST_MODIFIED, new Date(asset.lastModified()));
        return res.build();
    }
}
