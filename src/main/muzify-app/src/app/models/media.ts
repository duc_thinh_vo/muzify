export class Media {
  id: number;
  name: String;
  artist: String;
  genre: String;
  url: String;
}
