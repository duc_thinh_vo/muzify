(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./getFbSDK.js":
/*!*********************!*\
  !*** ./getFbSDK.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

window.fbAsyncInit = ()=> {
  FB.init({
    appId            : '551681558504367',
    xfbml            : false,
    version          : 'v2.9'
  });
  FB.AppEvents.logPageView();
};

(function(d, s, id){
  let js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/UserSession.ts":
/*!********************************!*\
  !*** ./src/app/UserSession.ts ***!
  \********************************/
/*! exports provided: UserSession */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserSession", function() { return UserSession; });
var TokenKey = 'TokenKey';
var UserIdKey = 'UserIdKey';
var UsernameKey = 'UserNameKey';
var EmailKey = 'EmailKey';
var UserSession = /** @class */ (function () {
    function UserSession() {
    }
    Object.defineProperty(UserSession, "accessToken", {
        get: function () {
            return localStorage.getItem(TokenKey);
        },
        // accessToken
        set: function (value) {
            localStorage.setItem(TokenKey, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserSession, "username", {
        get: function () {
            return localStorage.getItem(UsernameKey);
        },
        // username
        set: function (value) {
            localStorage.setItem(UsernameKey, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserSession, "userId", {
        get: function () {
            return localStorage.getItem(UserIdKey);
        },
        // userId
        set: function (value) {
            localStorage.setItem(UserIdKey, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserSession, "email", {
        get: function () {
            return localStorage.getItem(EmailKey);
        },
        // email
        set: function (value) {
            localStorage.setItem(EmailKey, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserSession, "isLoggedIn", {
        // isUserLoggedIn getter
        get: function () {
            if (UserSession.accessToken) {
                return true;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    UserSession.logout = function () {
        localStorage.removeItem(TokenKey);
        localStorage.removeItem(UserIdKey);
        localStorage.removeItem(UsernameKey);
    };
    UserSession.loginSuccessfullyWithDictionary = function (dict) {
        UserSession.accessToken = 'Bearer ' + dict['accessToken'];
        var userInfo = dict['user'];
        UserSession.updateWithNewInfo(userInfo);
    };
    UserSession.updateWithNewInfo = function (newInfo) {
        if (newInfo) {
            var userId = newInfo['user_id'];
            if (userId) {
                UserSession.userId = userId;
            }
            var email = newInfo['email'];
            if (email) {
                UserSession.email = email;
            }
            var username = newInfo['username'];
            if (username) {
                UserSession.username = username;
            }
        }
    };
    return UserSession;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/user-profile/user-profile.component */ "./src/app/components/user-profile/user-profile.component.ts");
/* harmony import */ var _components_upload_media_upload_media_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/upload-media/upload-media.component */ "./src/app/components/upload-media/upload-media.component.ts");
/* harmony import */ var _components_email_confirmation_email_confirmation_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/email-confirmation/email-confirmation.component */ "./src/app/components/email-confirmation/email-confirmation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    { path: 'login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'home', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'user-profile', component: _components_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_5__["UserProfileComponent"] },
    { path: 'upload-media', component: _components_upload_media_upload_media_component__WEBPACK_IMPORTED_MODULE_6__["UploadMediaComponent"] },
    { path: 'email-confirmation', component: _components_email_confirmation_email_confirmation_component__WEBPACK_IMPORTED_MODULE_7__["EmailConfirmationComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes),
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
            ],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-login *ngIf=\"!isUserLoggedIn && !emailConfirmation\"></app-login>\n<app-header *ngIf=\"isUserLoggedIn && !emailConfirmation\"></app-header>\n<app-nav *ngIf=\"isUserLoggedIn && !emailConfirmation\"></app-nav>\n<router-outlet></router-outlet>\n<app-music-player-widget *ngIf=\"isUserLoggedIn && !emailConfirmation\"></app-music-player-widget>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n\n:host {\n  /* Medium devices (landscape tablets, 768px and up) */\n  /* Large devices (laptops/desktops, 992px and up) */ }\n\n:host app-header, :host app-nav {\n    position: fixed;\n    left: 2%; }\n\n:host app-header {\n    right: 2%;\n    top: 5%; }\n\n:host app-nav {\n    display: none;\n    top: 15%; }\n\n:host app-music-player-widget {\n    position: fixed;\n    left: 5%;\n    right: 5%;\n    bottom: 5%; }\n\n@media only screen and (min-width: 992px) {\n    :host app-nav {\n      display: flex; }\n    :host app-music-player-widget {\n      left: 25%; } }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _UserSession__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserSession */ "./src/app/UserSession.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    // emailConfirmation: boolean;
    function AppComponent(router) {
        this.router = router;
        this.title = 'app';
    }
    Object.defineProperty(AppComponent.prototype, "isUserLoggedIn", {
        get: function () {
            return _UserSession__WEBPACK_IMPORTED_MODULE_1__["UserSession"].isLoggedIn;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent.prototype, "emailConfirmation", {
        get: function () {
            if (this.router.url === '/email-confirmation')
                return true;
        },
        enumerable: true,
        configurable: true
    });
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! .//app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_music_player_widget_music_player_widget_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/music-player-widget/music-player-widget.component */ "./src/app/components/music-player-widget/music-player-widget.component.ts");
/* harmony import */ var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/footer/footer.component */ "./src/app/components/footer/footer.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/user-profile/user-profile.component */ "./src/app/components/user-profile/user-profile.component.ts");
/* harmony import */ var _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/nav/nav.component */ "./src/app/components/nav/nav.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _components_upload_media_upload_media_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/upload-media/upload-media.component */ "./src/app/components/upload-media/upload-media.component.ts");
/* harmony import */ var _interceptors_token_interceptor__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./interceptors/token.interceptor */ "./src/app/interceptors/token.interceptor.ts");
/* harmony import */ var _components_search_bar_search_bar_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/search-bar/search-bar.component */ "./src/app/components/search-bar/search-bar.component.ts");
/* harmony import */ var _components_email_confirmation_email_confirmation_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/email-confirmation/email-confirmation.component */ "./src/app/components/email-confirmation/email-confirmation.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                _components_music_player_widget_music_player_widget_component__WEBPACK_IMPORTED_MODULE_7__["MusicPlayerWidgetComponent"],
                _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_9__["HeaderComponent"],
                _components_user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_10__["UserProfileComponent"],
                _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_11__["NavComponent"],
                _components_upload_media_upload_media_component__WEBPACK_IMPORTED_MODULE_13__["UploadMediaComponent"],
                _components_search_bar_search_bar_component__WEBPACK_IMPORTED_MODULE_15__["SearchBarComponent"],
                _components_email_confirmation_email_confirmation_component__WEBPACK_IMPORTED_MODULE_16__["EmailConfirmationComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClientModule"]
            ],
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HTTP_INTERCEPTORS"], useClass: _interceptors_token_interceptor__WEBPACK_IMPORTED_MODULE_14__["TokenInterceptor"], multi: true },
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_17__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_17__["HashLocationStrategy"] }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/email-confirmation/email-confirmation.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/email-confirmation/email-confirmation.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p *ngIf=\"confirmationCode==0\" class=\"confirmationMessage\">Confirmation is not valid.<br>We have sent a new activation email.</p>\n<p *ngIf=\"confirmationCode==1\" class=\"confirmationMessage\">Your email address has been confirmed.</p>\n"

/***/ }),

/***/ "./src/app/components/email-confirmation/email-confirmation.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/components/email-confirmation/email-confirmation.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n\n:host {\n  position: absolute;\n  left: 50%;\n  top: 20%;\n  width: 400px;\n  height: 200px;\n  margin-left: -200px;\n  border: 1px solid #E8D7F1;\n  border-radius: 10px; }\n\n:host p {\n    color: #663C70;\n    text-align: center;\n    position: relative;\n    top: 50%;\n    -webkit-transform: translateY(-50%);\n            transform: translateY(-50%); }\n"

/***/ }),

/***/ "./src/app/components/email-confirmation/email-confirmation.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/email-confirmation/email-confirmation.component.ts ***!
  \*******************************************************************************/
/*! exports provided: EmailConfirmationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailConfirmationComponent", function() { return EmailConfirmationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmailConfirmationComponent = /** @class */ (function () {
    function EmailConfirmationComponent(route) {
        this.route = route;
        this.confirmationCode = 0;
    }
    EmailConfirmationComponent.prototype.ngOnInit = function () {
        // this.route.queryParams.subscribe(params => {
        // this.activationCode = params['activationCode'];
        this.activationCode = this.route.snapshot.queryParamMap.get('activationCode');
        // })
    };
    EmailConfirmationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-email-confirmation',
            template: __webpack_require__(/*! ./email-confirmation.component.html */ "./src/app/components/email-confirmation/email-confirmation.component.html"),
            styles: [__webpack_require__(/*! ./email-confirmation.component.scss */ "./src/app/components/email-confirmation/email-confirmation.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], EmailConfirmationComponent);
    return EmailConfirmationComponent;
}());



/***/ }),

/***/ "./src/app/components/footer/footer.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/footer/footer.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n"

/***/ }),

/***/ "./src/app/components/footer/footer.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/footer/footer.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n"

/***/ }),

/***/ "./src/app/components/footer/footer.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/footer/footer.component.ts ***!
  \*******************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/components/footer/footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<ul>-->\n  <!--<li><button (click)=\"onLogoClick()\">Muzify</button></li>-->\n  <!--<li *ngIf=\"!isUserLoggedIn\"><button (click)=\"onLoginClick()\">Log In</button></li>-->\n  <!--&lt;!&ndash;<li><button (click)=\"onUploadMediaClick()\" class=\"fas fa-cloud-upload-alt\"></button></li>&ndash;&gt;-->\n  <!--<li><button (click)=\"onUserProfileClick()\"><i class=\"fas fa-user\"></i></button></li>-->\n<!--</ul>-->\n<!--<div class=\"header\">-->\n  <button *ngIf=\"isUserLoggedIn\" id=\"button-list\" class=\"fas fa-bars\"></button>\n  <button *ngIf=\"isUserLoggedIn\" id=\"button-home\" (click)=\"onLogoClick()\">Muzify</button>\n  <app-search-bar *ngIf=\"isUserLoggedIn\"></app-search-bar>\n  <!--<li><button (click)=\"onUploadMediaClick()\" class=\"fas fa-cloud-upload-alt\"></button></li>-->\n  <button *ngIf=\"isUserLoggedIn\" id=\"button-user-profile\" (click)=\"onUserProfileClick()\"><i class=\"fas fa-user\"></i></button>\n<!--</div>-->\n\n"

/***/ }),

/***/ "./src/app/components/header/header.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n\n:host {\n  display: flex;\n  /* Large devices (laptops/desktops, 992px and up) */ }\n\n:host button {\n    font-size: 80%; }\n\n@media only screen and (min-width: 992px) {\n    :host #button-list {\n      display: none; } }\n"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _UserSession__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../UserSession */ "./src/app/UserSession.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router) {
        this.router = router;
    }
    Object.defineProperty(HeaderComponent.prototype, "isUserLoggedIn", {
        get: function () {
            return _UserSession__WEBPACK_IMPORTED_MODULE_2__["UserSession"].isLoggedIn;
        },
        enumerable: true,
        configurable: true
    });
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.onLogoClick = function () {
        this.router.navigate(['home']);
    };
    HeaderComponent.prototype.onLoginClick = function () {
        this.router.navigate(['login']);
    };
    HeaderComponent.prototype.onUserProfileClick = function () {
        this.router.navigate(['user-profile']);
    };
    HeaderComponent.prototype.onUploadMediaClick = function () {
        this.router.navigate(['upload-media']);
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/components/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/home/home.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _UserSession__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../UserSession */ "./src/app/UserSession.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(router) {
        this.router = router;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(HomeComponent.prototype, "isUserLoggedIn", {
        get: function () {
            return _UserSession__WEBPACK_IMPORTED_MODULE_1__["UserSession"].isLoggedIn;
        },
        enumerable: true,
        configurable: true
    });
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/components/home/home.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<form text-center (submit)=\"emitLoginEvent()\">-->\n\n  <!--<input [(ngModel)]=\"user.username\" name=\"username\" type=\"text\" placeholder=\"Username\" #username=\"ngModel\"-->\n         <!--required>-->\n\n  <!--<input [(ngModel)]=\"user.password\" name=\"password\" type=\"password\" placeholder=\"Password\" #password=\"ngModel\"-->\n         <!--required>-->\n\n  <!--<button type=\"submit\" [disabled]=\"!username.valid || !password.valid\">Login</button>-->\n\n  <!--<h6 text-center>{{status}}</h6>-->\n<!--</form>-->\n\n\n<form class=\"login-form\">\n  <input type=\"text\" ng-model=\"username\" placeholder=\"username\">\n  <input type=\"text\" ng-model=\"password\" placeholder=\"password\">\n  <!--<button type=\"submit\" [disabled]=\"!username.valid || !password.valid\">Log In</button>-->\n  <button type=\"submit\">Log In</button>\n\n</form>\n<button (click)=\"onLoginClick()\">Login with Facebook</button>\n"

/***/ }),

/***/ "./src/app/components/login/login.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n\n:host {\n  height: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center; }\n\n:host input, :host button {\n    font-size: 60%;\n    margin: 8px;\n    border: 1px solid #8F6593;\n    border-radius: 5px; }\n\n:host form {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center; }\n\n:host form input {\n      width: 100%;\n      padding: 12px 20px;\n      background-color: white; }\n\n:host form button {\n      width: 50%;\n      padding: 8px;\n      background-color: #663C70; }\n\n:host button {\n    margin-top: 10%;\n    padding: 8px;\n    background-color: #3B5998;\n    border-color: white;\n    color: white; }\n"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_user_data_user_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/user-data/user-data.service */ "./src/app/services/user-data/user-data.service.ts");
/* harmony import */ var _UserSession__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../UserSession */ "./src/app/UserSession.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, userDataService) {
        this.router = router;
        this.userDataService = userDataService;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onLoginClick = function () {
        var _this = this;
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                console.log(response.authResponse.accessToken);
                _this.userDataService.loginViaFacebook(response.authResponse.accessToken).subscribe(function (serverResponse) {
                    _UserSession__WEBPACK_IMPORTED_MODULE_3__["UserSession"].loginSuccessfullyWithDictionary(serverResponse);
                    console.log(serverResponse);
                });
                _this.router.navigate(['home']);
            }
            else {
                FB.login(function (loginResponse) {
                    console.log(response.authResponse.accessToken);
                    _this.userDataService.loginViaFacebook(loginResponse.authResponse.accessToken).subscribe(function (serverResponse) {
                        _UserSession__WEBPACK_IMPORTED_MODULE_3__["UserSession"].loginSuccessfullyWithDictionary(serverResponse);
                    });
                    _this.router.navigate(['home']);
                });
            }
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/components/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_user_data_user_data_service__WEBPACK_IMPORTED_MODULE_2__["UserDataService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/music-player-widget/music-player-widget.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/music-player-widget/music-player-widget.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<img (click)=\"onImageClick()\" src=\"../../../assets/images/background.jpeg\">\n\n<div class=\"media-controller\">\n  <div class=\"info\">\n    <p>Name</p>\n    <p>Artist</p>\n  </div>\n\n  <div class=\"controller\">\n    <button class=\"ion-ios-shuffle\"></button>\n    <button class=\"fas fa-backward\"></button>\n    <button id=\"button-play\" class=\"fas fa-play\"></button>\n    <button hidden class=\"ion-ios-pause\"></button>\n    <button class=\"fas fa-forward\"></button>\n    <button class=\"ion-ios-repeat\"></button>\n  </div>\n</div>\n\n<input class=\"slider\" type=\"range\" min=\"0\" max=\"100\" value=\"0\">\n\n\n\n\n"

/***/ }),

/***/ "./src/app/components/music-player-widget/music-player-widget.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/components/music-player-widget/music-player-widget.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n\n:host {\n  height: 100px;\n  background-color: #E8D7F1;\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  font-size: 80%;\n  border-radius: 50px; }\n\n:host img {\n    height: 120px;\n    width: 120px;\n    border-radius: 100%; }\n\n:host .media-controller {\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    width: 60%;\n    margin-left: 5%; }\n\n:host .media-controller .info {\n      display: flex;\n      justify-content: center;\n      flex-direction: column;\n      align-items: center; }\n\n:host .media-controller .controller {\n      display: flex;\n      flex-direction: row;\n      justify-content: space-between; }\n\n:host .media-controller .controller button {\n        font-size: 80%; }\n\n:host .media-controller .controller button:hover {\n        color: #8F6593; }\n\n:host .media-controller .controller #button-play {\n        font-size: 140%; }\n\n:host .slider {\n    display: none; }\n\n:host .slider::-webkit-slider-thumb {\n    display: none; }\n\n@media only screen and (min-width: 600px) {\n    :host .media-controller {\n      width: 70%; } }\n\n@media only screen and (min-width: 992px) {\n    :host {\n      justify-content: space-between; }\n      :host .media-controller {\n        margin-left: 0; }\n      :host .slider {\n        display: flex;\n        -webkit-appearance: none;\n        height: 3px;\n        background: #663C70;\n        margin-right: 10px;\n        width: 120px; }\n      :host .slider::-webkit-slider-thumb {\n        display: flex;\n        -webkit-appearance: none;\n                appearance: none;\n        width: 10px;\n        height: 10px;\n        background: #8F6593;\n        cursor: pointer;\n        border-radius: 50%; } }\n"

/***/ }),

/***/ "./src/app/components/music-player-widget/music-player-widget.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/music-player-widget/music-player-widget.component.ts ***!
  \*********************************************************************************/
/*! exports provided: MusicPlayerWidgetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MusicPlayerWidgetComponent", function() { return MusicPlayerWidgetComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MusicPlayerWidgetComponent = /** @class */ (function () {
    function MusicPlayerWidgetComponent() {
    }
    MusicPlayerWidgetComponent.prototype.ngOnInit = function () {
    };
    MusicPlayerWidgetComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-music-player-widget',
            template: __webpack_require__(/*! ./music-player-widget.component.html */ "./src/app/components/music-player-widget/music-player-widget.component.html"),
            styles: [__webpack_require__(/*! ./music-player-widget.component.scss */ "./src/app/components/music-player-widget/music-player-widget.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MusicPlayerWidgetComponent);
    return MusicPlayerWidgetComponent;
}());



/***/ }),

/***/ "./src/app/components/nav/nav.component.html":
/*!***************************************************!*\
  !*** ./src/app/components/nav/nav.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>YOUR MUSIC</p>\n<button><i class=\"fas fa-music\"></i> Songs</button>\n<button><i class=\"fas fa-images\"></i> Albums</button>\n<button><i class=\"fas fa-users\"></i> Artists</button>\n<p>PLAYLISTS</p>\n"

/***/ }),

/***/ "./src/app/components/nav/nav.component.scss":
/*!***************************************************!*\
  !*** ./src/app/components/nav/nav.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n\n:host {\n  display: flex;\n  flex-direction: column;\n  border-right: 1px solid #663C70;\n  padding-right: 5%;\n  height: 100%; }\n\n:host button {\n    text-align: start;\n    font-size: 80%;\n    padding: 15%; }\n"

/***/ }),

/***/ "./src/app/components/nav/nav.component.ts":
/*!*************************************************!*\
  !*** ./src/app/components/nav/nav.component.ts ***!
  \*************************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavComponent = /** @class */ (function () {
    function NavComponent() {
    }
    NavComponent.prototype.ngOnInit = function () {
    };
    NavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nav',
            template: __webpack_require__(/*! ./nav.component.html */ "./src/app/components/nav/nav.component.html"),
            styles: [__webpack_require__(/*! ./nav.component.scss */ "./src/app/components/nav/nav.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NavComponent);
    return NavComponent;
}());



/***/ }),

/***/ "./src/app/components/search-bar/search-bar.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/search-bar/search-bar.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input type=\"text\" placeholder=\"Search..\" name=\"search\">\n<button type=\"submit\"><i class=\"fa fa-search\"></i></button>\n"

/***/ }),

/***/ "./src/app/components/search-bar/search-bar.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/search-bar/search-bar.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body {\n  margin: 0;\n  height: 100%;\n  width: 100%;\n  font-family: 'Source Code Pro', sans-serif;\n  font-size: 120%; }\n\np, h1, h2, h3, h4, h5, h6 {\n  margin: 0; }\n\nbutton {\n  border: none;\n  color: #663C70;\n  background-color: transparent; }\n\nbutton:hover {\n  color: #E8D7F1; }\n\n:host {\n  display: flex;\n  align-items: center;\n  margin: 0 10%; }\n\ninput, button {\n  border-radius: 5px; }\n\ninput {\n  border: 1px solid #E8D7F1;\n  padding: 4% 10%; }\n\nbutton {\n  border: none;\n  background-color: inherit; }\n"

/***/ }),

/***/ "./src/app/components/search-bar/search-bar.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/search-bar/search-bar.component.ts ***!
  \***************************************************************/
/*! exports provided: SearchBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchBarComponent", function() { return SearchBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SearchBarComponent = /** @class */ (function () {
    function SearchBarComponent() {
    }
    SearchBarComponent.prototype.ngOnInit = function () {
    };
    SearchBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-search-bar',
            template: __webpack_require__(/*! ./search-bar.component.html */ "./src/app/components/search-bar/search-bar.component.html"),
            styles: [__webpack_require__(/*! ./search-bar.component.scss */ "./src/app/components/search-bar/search-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SearchBarComponent);
    return SearchBarComponent;
}());



/***/ }),

/***/ "./src/app/components/upload-media/upload-media.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/upload-media/upload-media.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form>\n  Name <input ng-model=\"media.name\">\n  <input type=\"file\">\n</form>\n\n<button>Upload</button>\n\n"

/***/ }),

/***/ "./src/app/components/upload-media/upload-media.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/upload-media/upload-media.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/upload-media/upload-media.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/upload-media/upload-media.component.ts ***!
  \*******************************************************************/
/*! exports provided: UploadMediaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadMediaComponent", function() { return UploadMediaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_media_data_media_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/media-data/media-data.service */ "./src/app/services/media-data/media-data.service.ts");
/* harmony import */ var _services_user_data_user_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/user-data/user-data.service */ "./src/app/services/user-data/user-data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UploadMediaComponent = /** @class */ (function () {
    function UploadMediaComponent(mediaDataService, userDataService) {
        this.mediaDataService = mediaDataService;
        this.userDataService = userDataService;
        this.media = {
            name: '',
            file: null
        };
    }
    UploadMediaComponent.prototype.ngOnInit = function () {
    };
    UploadMediaComponent.prototype.uploadMedia = function () {
        this.mediaDataService.uploadMedia(this.media);
    };
    UploadMediaComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-upload-media',
            template: __webpack_require__(/*! ./upload-media.component.html */ "./src/app/components/upload-media/upload-media.component.html"),
            styles: [__webpack_require__(/*! ./upload-media.component.scss */ "./src/app/components/upload-media/upload-media.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_media_data_media_data_service__WEBPACK_IMPORTED_MODULE_1__["MediaDataService"],
            _services_user_data_user_data_service__WEBPACK_IMPORTED_MODULE_2__["UserDataService"]])
    ], UploadMediaComponent);
    return UploadMediaComponent;
}());



/***/ }),

/***/ "./src/app/components/user-profile/user-profile.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/user-profile/user-profile.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<audio controls *ngFor=\"let item of mediaArray\">\n  <source [src]=\"item.url\" type=\"audio/ogg\">\n  <source [src]=\"item.url\" type=\"audio/mpeg\">\n</audio>\n"

/***/ }),

/***/ "./src/app/components/user-profile/user-profile.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/user-profile/user-profile.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/user-profile/user-profile.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/user-profile/user-profile.component.ts ***!
  \*******************************************************************/
/*! exports provided: UserProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfileComponent", function() { return UserProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_media_data_media_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/media-data/media-data.service */ "./src/app/services/media-data/media-data.service.ts");
/* harmony import */ var _services_user_data_user_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/user-data/user-data.service */ "./src/app/services/user-data/user-data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserProfileComponent = /** @class */ (function () {
    function UserProfileComponent(mediaDataService, userDataService) {
        this.mediaDataService = mediaDataService;
        this.userDataService = userDataService;
    }
    UserProfileComponent.prototype.ngOnInit = function () {
        this.downloadMediasOfCurrentUser();
    };
    UserProfileComponent.prototype.downloadMediasOfCurrentUser = function () {
        var _this = this;
        this.mediaDataService.getMediasOfCurrentUser().subscribe(function (response) {
            _this.mediaArray = response;
            console.log(response);
        });
    };
    UserProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-profile',
            template: __webpack_require__(/*! ./user-profile.component.html */ "./src/app/components/user-profile/user-profile.component.html"),
            styles: [__webpack_require__(/*! ./user-profile.component.scss */ "./src/app/components/user-profile/user-profile.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_media_data_media_data_service__WEBPACK_IMPORTED_MODULE_1__["MediaDataService"],
            _services_user_data_user_data_service__WEBPACK_IMPORTED_MODULE_2__["UserDataService"]])
    ], UserProfileComponent);
    return UserProfileComponent;
}());



/***/ }),

/***/ "./src/app/interceptors/token.interceptor.ts":
/*!***************************************************!*\
  !*** ./src/app/interceptors/token.interceptor.ts ***!
  \***************************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _UserSession__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../UserSession */ "./src/app/UserSession.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor() {
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        var token = _UserSession__WEBPACK_IMPORTED_MODULE_1__["UserSession"].accessToken;
        if (token && token.trim() !== '') {
            request = request.clone({
                setHeaders: {
                    'Authorization': "" + token
                }
            });
            return next.handle(request);
        }
        return next.handle(request);
    };
    TokenInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/services/media-data/media-data.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/media-data/media-data.service.ts ***!
  \***********************************************************/
/*! exports provided: MediaDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MediaDataService", function() { return MediaDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _UserSession__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../UserSession */ "./src/app/UserSession.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MediaDataService = /** @class */ (function () {
    function MediaDataService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].API_BASE_URL;
        this.uploadUrl = this.apiUrl + "/songs";
        this.mediasOfCurrentUserUrl = this.apiUrl + "/songs/me";
    }
    MediaDataService.prototype.uploadMedia = function (formData) {
        return this.http.post(this.uploadUrl, formData);
    };
    MediaDataService.prototype.getMediasOfCurrentUser = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Authorization', _UserSession__WEBPACK_IMPORTED_MODULE_3__["UserSession"].accessToken);
        return this.http.get(this.mediasOfCurrentUserUrl, { headers: headers });
    };
    MediaDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], MediaDataService);
    return MediaDataService;
}());



/***/ }),

/***/ "./src/app/services/user-data/user-data.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/user-data/user-data.service.ts ***!
  \*********************************************************/
/*! exports provided: UserDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDataService", function() { return UserDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserDataService = /** @class */ (function () {
    function UserDataService(http) {
        this.http = http;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].API_BASE_URL;
        this.signUpViaFacebookUrl = this.apiUrl + "/users";
    }
    UserDataService.prototype.loginViaFacebook = function (token) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]()
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .set('Authorization', _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].Authorization);
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('access_token', token);
        return this.http.post(this.signUpViaFacebookUrl, body.toString(), { headers: headers });
    };
    UserDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserDataService);
    return UserDataService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    API_BASE_URL: 'https://muzify.eu/api',
    Authorization: 'Basic dGhpbmh2bzoxMjM0NTY=',
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _getFbSDK_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../getFbSDK.js */ "./getFbSDK.js");
/* harmony import */ var _getFbSDK_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_getFbSDK_js__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/hongngocdoan/Documents/Work/muzify/src/main/muzify-app/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map