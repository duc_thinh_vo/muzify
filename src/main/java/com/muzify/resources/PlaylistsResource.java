/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

import com.muzify.dao.OauthUserFacade;
import com.muzify.dao.PlaylistFacade;
import com.muzify.dao.SongFacade;
import com.muzify.domain.Artist;
import com.muzify.domain.OauthUser;
import com.muzify.domain.Playlist;
import com.muzify.domain.Song;
import com.muzify.dynamicfeature.TokenAuthenticated;
import com.muzify.exception.ErrorMessage;
import com.muzify.exception.MissingParamException;
import com.muzify.exception.NotAuthorizedException;
import com.muzify.exception.NotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author thinh
 */
@Path("playlists")
public class PlaylistsResource {

    @Context
    private UriInfo context;
    
    @EJB
    OauthUserFacade oauthUserFacade;
    
    @EJB
    PlaylistFacade playlistFacade;
    
    @EJB
    SongFacade songFacade;
    
    public PlaylistsResource() {
    }

    @GET
    @TokenAuthenticated
    @Path("me")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlaylists() {
        // TODO: Use filter to receive the user
        OauthUser currentUser = oauthUserFacade.find("49d186d6-b592-407a-8ab7-8cec9fbe1a32");
        if (currentUser != null) {
            Collection<Playlist> playlists = currentUser.getPlaylistCollection();
            GenericEntity<Collection<Playlist>> genericEntity = new GenericEntity<Collection<Playlist>>(playlists){};
            return Response.status(200).entity(genericEntity).build();
        }
        
        ErrorMessage errorMessage = new ErrorMessage("", "Unable to create a playlist", "", Response.Status.BAD_REQUEST);
        throw new MissingParamException(errorMessage);
    }
    
    @GET
    @TokenAuthenticated
    @Path("{user_uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlaylistByUserUUID(@PathParam("user_uuid") String userUUID) {
        OauthUser user = oauthUserFacade.find(userUUID);
        if (user == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Unable to find user by the given UUID", "", Response.Status.NOT_FOUND);
            throw new MissingParamException(errorMessage);
        }
        
        Collection<Playlist> playlists = user.getPlaylistCollection();
        GenericEntity<Collection<Playlist>> entity = new GenericEntity<Collection<Playlist>>(playlists){};
        return Response.status(200).entity(entity).build();
    }

    @PUT
    @TokenAuthenticated
    @Path("{playlist_id}/songs")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePlaylist(@PathParam("playlist_id") Long playlistId,
                                   @FormParam("song_ids") List<Long> songIds,
                                   @FormParam("name") String playlistName) {
        // TODO: Use filter to receive the user
        OauthUser currentUser = oauthUserFacade.find("49d186d6-b592-407a-8ab7-8cec9fbe1a32");
        if (currentUser == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "An unauthrorized operation!", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        if (playlistId == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Missing valid playlist_id.", "", Response.Status.BAD_REQUEST);
            throw new MissingParamException(errorMessage);
        }
        
        Playlist playlist = playlistFacade.find(playlistId);
        
        if (playlistName != null) {
            // Do not save to db yet. Because
            playlist.setName(playlistName);
        }
        
        if (playlist == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Unable to fine playlist by the given id", "", Response.Status.NOT_FOUND);
            throw new NotFoundException(errorMessage);
        }
        
        if (!playlist.getUserUuid().getUuid().equals(currentUser.getUuid())) {
            ErrorMessage errorMessage = new ErrorMessage("", "An unauthrorized operation. You are not allowed to update this play list", "", Response.Status.UNAUTHORIZED);
            throw new NotAuthorizedException(errorMessage);
        }
        
        if (songIds != null && songIds.size() > 0) {
            List<Song> songs = new ArrayList();
            for (Long songId: songIds) {
                Song song = songFacade.find(songId);
                if (song == null) {
                    ErrorMessage errorMessage = new ErrorMessage("", "The song_ids list contains incorrect song id " + songId, "", Response.Status.NOT_FOUND);
                    throw new NotFoundException(errorMessage);
                }

                if (!song.getOwners().contains(currentUser)) {
                    ErrorMessage errorMessage = new ErrorMessage("", "You are not allowed to add song with id " + songId + " to your play list", "", Response.Status.UNAUTHORIZED);
                    throw new NotAuthorizedException(errorMessage);
                }

                songs.add(song);
            }
            
            for (Song song: songs) {
                List<Playlist> playlists = song.getPlaylists();
                playlists.add(playlist);
                song.setPlaylists(playlists);
                songFacade.edit(song);
            }
            
            playlist.setSongs(songs);
            playlistFacade.edit(playlist);
        }
        
        if (playlistName != null && !playlistName.trim().equals("")) {
            playlist.setName(playlistName);
            playlistFacade.edit(playlist);
        }
        
        return Response.status(200).entity(playlist).build();
    }
    
    @POST
    @TokenAuthenticated
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPlaylist(@FormParam("name") String name) {
        if (name == null || name.trim().equals("")) {
            ErrorMessage errorMessage = new ErrorMessage("", "Missing name parameter", "", Response.Status.BAD_REQUEST);
            throw new MissingParamException(errorMessage);
        }
        
        // TODO: The user object should be returned from filter
        OauthUser currentUser = oauthUserFacade.find("49d186d6-b592-407a-8ab7-8cec9fbe1a32");
        
        if (currentUser != null) {
            Playlist playlist = new Playlist();
            playlist.setUserUuid(currentUser);
            playlist.setName(name.trim());
            
            playlistFacade.create(playlist);
            return Response.status(200).entity(playlist).build();
        }
        
        ErrorMessage errorMessage = new ErrorMessage("", "Unable to create a playlist", "", Response.Status.BAD_REQUEST);
        throw new MissingParamException(errorMessage);
    }
}
