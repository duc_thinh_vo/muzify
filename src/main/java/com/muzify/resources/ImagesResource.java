/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

/**
 *
 * @author thinh
 */
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import javax.ejb.Stateless;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.muzify.exception.ErrorMessage;
import com.muzify.exception.NotFoundException;
import com.muzify.utils.ImageFileManager;
import java.io.File;
import java.io.IOException;


/**
 * REST Web Service
 *
 * @author thinh
 */
@Stateless
@Path("/images")
@Api(value = "/images", description="Gets image by image file name")
public class ImagesResource {
    
    @GET
    @Path("{filename}")
    @Produces({"image/png", "image/jpg", "image/gif"})
    @ApiOperation(value = "Gets a specific user", notes = "Fetches the user's details")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 401, message = "Unauthorized request"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response getImage(@PathParam("filename") String fileName) throws NotFoundException, IOException {
        ImageFileManager ifm = new ImageFileManager();
        
        File imageFile = new File(ifm.storageFilePath() + fileName);
        if (fileName == null || fileName.equals("") || !imageFile.exists()) {
            ErrorMessage message = new ErrorMessage("", "The requested image is not found", "", Response.Status.NOT_FOUND);
            throw new NotFoundException(message);
        }
        
        Response.ResponseBuilder responseBuilder = Response.ok((Object) imageFile);
        responseBuilder.header("Content-Disposition", "attachment; filename=\"" + fileName +"\"");
        return responseBuilder.build();
    }
}
