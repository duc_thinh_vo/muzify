import { Component, OnInit } from '@angular/core';
import {UserSession} from '../../UserSession';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  public get isUserLoggedIn(): boolean {
    return UserSession.isLoggedIn;
  }
}
