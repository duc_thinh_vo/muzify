import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-music-player-widget',
  templateUrl: './music-player-widget.component.html',
  styleUrls: ['./music-player-widget.component.scss']
  // animations: [
  //   trigger('music-player-widget-state', [
  //     state('roll-in', style({
  //       width: '0',
  //       transform: 'scale(1)'
  //     })),
  //     state('roll-out', style({
  //       width: '100%',
  //       transform: 'scale(1.1)'
  //     })),
  //     transition('roll-out => roll-in', animate('100ms ease-in')),
  //     transition('roll-in => roll-out', animate('100ms ease-out'))
  //   ])
  // ]
})

export class MusicPlayerWidgetComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }

  // toggleState() {
  //   this.state = this.state === 'roll-in' ? 'roll-out' : 'roll-in';
  // }
}
