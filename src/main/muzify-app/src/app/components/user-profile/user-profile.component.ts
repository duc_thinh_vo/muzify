import { Component, OnInit } from '@angular/core';
import { MediaDataService } from '../../services/media-data/media-data.service';
import { UserDataService } from '../../services/user-data/user-data.service';
import {Media} from '../../models/media';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  mediaArray: any[];

  constructor(private mediaDataService: MediaDataService,
              private userDataService: UserDataService) { }

  ngOnInit() {
    this.downloadMediasOfCurrentUser();
  }

  downloadMediasOfCurrentUser() {
    this.mediaDataService.getMediasOfCurrentUser().subscribe(response => {
      this.mediaArray = response as any[];
      console.log(response);
    });
  }
}
