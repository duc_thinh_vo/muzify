/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.resources;

/**
 *
 * @author thinh
 */
import com.muzify.dao.GenreFacade;
import com.muzify.domain.Genre;
import com.muzify.domain.OauthUser;
import com.muzify.dynamicfeature.TokenAuthenticated;
import com.muzify.exception.ErrorMessage;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import javax.ejb.Stateless;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.muzify.exception.NotFoundException;
import com.muzify.hk2factory.OauthUserFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;

@Stateless
@Path("/genres")
@Api(value = "/genres", description="Genres endpoints")
public class GenresResource {
    
    @EJB
    private GenreFacade genreFacade;
    
    @Context 
    ContainerRequestContext requestContext;
            
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Gets a specific genre", notes = "Returns a specify genre")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 401, message = "Unauthorized request"),
        @ApiResponse(code = 404, message = "Not found"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response getGenre(@PathParam("id") Integer id) throws NotFoundException {
        Genre genre = genreFacade.find(id);
        if (genre == null) {
            ErrorMessage errorMessage = new ErrorMessage("", "Unable to find genre by the given id", "", Response.Status.NOT_FOUND);
            throw new NotFoundException(errorMessage);
        }
        
        return Response.status(200).entity(genre).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Gets all genres or search genres", notes = "Get all genres or search genres. Add name parameter in order to do the searching")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 401, message = "Unauthorized request"),
        @ApiResponse(code = 404, message = "Not found"),
        @ApiResponse(code = 500, message = "Internal server error")})
    public Response getAllGenres(@QueryParam("name") String name) {
        if (name == null || name.trim().equals("")) {
            List<Genre> genres = genreFacade.findAll();
            GenericEntity<List<Genre>> genericEntity = new GenericEntity<List<Genre>>(genres){};
            return Response.status(200).entity(genericEntity).build();
        }
        
        List<Genre> genres = genreFacade.searchGenresByName(name);
        GenericEntity<List<Genre>> genericEntity = new GenericEntity<List<Genre>>(genres){};
        return Response.status(200).entity(genericEntity).build();
    }
}
