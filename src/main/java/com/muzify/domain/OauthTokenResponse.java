/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muzify.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author thinh
 */
@XmlRootElement
public class OauthTokenResponse {
    private String accessToken;
    private Integer expire;
    private String scope;
    private String tokenType;
    private String refreshToken;
    private OauthUser user;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getExpire() {
        return expire;
    }

    public void setExpire(Integer expire) {
        this.expire = expire;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public OauthUser getUser() {
        return user;
    }

    public void setUser(OauthUser user) {
        this.user = user;
    }
}
